#food
A Bomb Calorimeter consists of a few main parts:

The Calorimeter
:   which is a large copper cylinder.

The Bomb
:   which is a smaller cylinder which sits inside the Calorimeter. This
    is where the substance is burnt.

The Bomb is connected to 2 fuse wires to set it off. There is also an
oxygen valve for the bomb. Between the Bomb and the Calorimeter is
filled with water, with a mechanical stirrer, and a thermometer. Then,
when we set off the fuse, that ignites the substance, and it burns.
Then, it loses the heat to the environment, which heats the water and
the thermometer. This is much more accurate because all of the heat goes
to the water, no matter how it burns, wheras in our experiment lots of
the heat escaped to the room, which we felt as it got hot.