#food
# Burning Crisps

  Crisp Type      Mass Change(g)   Initial Temp(°C)   End Temp(°C)   Energy Given Off (J 3sf)
  --------------- ---------------- ------------------ -------------- --------------------------
  Wotsits         0.38             22                 33             3040
  Ready Salted    0.44             24                 35             2630
  Mini Cheddars   0.76             24                 40             2210

# Bomb Calorimeters
![[Bomb Calorimeter]]
