#food
1.  Firstly, you pasteurise the milk at 85-95 degrees Celsius, in order
    to kill any bacteria that might be in the milk.
2.  Then, you homogenise the milk to ensure the fat globules are spread
    evenly, so the bacteria that we add in will spread out.
3.  After that, you cool the milk to 40 degrees, to ensure the
    temperature is perfect for the lactobactillus enzymes.
4.  Quickly start a timer for several hours, and put a thermometer into
    the mix. We are fermenting it, and try to stir it. This allows the
    lactobactillus to covert lactose and water into lactic acid. This
    lowers the pH of the milk, which then causes the proteins in the
    milk to coagulate as they denature. This causes the yoghurt to
    thicken.
5.  Then feel free to add flavours and colours.
6.  Finally, the yoghurt is stirred and cooled to 5 degrees to slow
    microbial growth/reproduction. Low pH also acts as a preservative.
