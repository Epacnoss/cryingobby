#biology #nerves #science
#  Explain how neurotransmitters play a key role in the transmission of electrical impulses at Synapses.

Firstly, an electrical impulse travels along the axon (The long (up to 1m) part of a nerve cell which the signal quickly travels across) of the first neuron. Then, when the nerve impulse reaches the dendrites (small extensions found on the end of the axon to connect neurons at synapses) at the end of the axon, these neurotransmitters are released. They are chemical messengers. They diffuse across the gap between the axons, and they bind with the receptor molecules on the second neuron. The receptors on the seocnd neuron can only bind to the specific neurotransmitters from the first one, in order to avoid signals going down the wrong neurons when nerves are close. The binding of the neurotransmitters stimulates the second neuron to transmit its own electrical impulse.


So, the electrical impulse travels down one neuron, which produces neurotransmitters, which bind with the second neuron and then it produces it's own signal.