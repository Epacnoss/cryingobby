#dt

# Exam Qs
## RM Jan 13
![[CAD Question]]
## RM June 15
![[Discuss the environmental implications of packaging and transporting large, assembled products around the world]]

# General
## LCAs
### Hanging Rail
![[Hanging Rail LCA]]
### Kritter Chair
![[Kritter Chair LCA]]

## Plastics
![[ProsCons of Plastics for Env]]
## Metals
![[Metal Extraction]]

## Timbers
![[Wooden Train - Batch Production]]