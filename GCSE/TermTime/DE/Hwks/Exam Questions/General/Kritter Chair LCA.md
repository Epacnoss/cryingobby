# Homework
## Raw Materials

Here, we have three main materials - the wood, the paint, the screws and
the polystyrene back. The screws are likely made of stainless steel or a
similar material in order to avoid rust. Steel is made from mainly iron
and carbon. This means that we have to mine iron and coal out of the
ground, and smelt it to create steel. This takes lots of energy. Next,
we have the paint. This is acrylic paint. This isn\'t as intensive to
create as Steel, but it needs a pigment and a resin. We also have the
wood - on the IKEA product page, it states that it is Solid Rubberwood.
Since this is a relatively middle-quality chair, we can assume that it
is made from quartersawn wood, which does have a small amount of waste.
However, it has a larger environmental aspect, as we are cutting trees
which help the environment. Finally, the polystyrene back is made from
polystyrene which is a plastic. This means we have had to drill or frack
for oil, and refine it, before being able to use it.

## Transport

All of this still has to be transported to the factory for manufacture
though. The wood can be transported relatively easily - we can just
strap logs or planks to a truck, and send them off, and typically wood
is harvested in the country it is used in, because that is cheaper. It
is also better for the environment, as we do not have to transport it
via plane or container ship (much more likely a container ship, if it
was used though due to the exorbitant cost of air freight). Then, for
the paint, lots of the components are in powder form, and any IKEA
factory likely has a deal with a paint shop/business with an outlet
nearby for them to get paint from. Then, depending on the location of
the factory either the screws are imported via trucks and ship, or just
moved via trucks. Finally, the polystyrene is likely already in the
polystyrene form, due to the complexities of oil refinement, which may
mean that we lose some efficiency due to the fact that typically
plastics aren\'t very dense, but we can still truck it from place to
place.

## Manufacture

Next, we need to take our wood, plastic, paint and screws and turn it
into a finished (or flatpacked to increase distribution efficiency)
chair. Firstly, we need to cut our wood to size, and this will require
heavy-duty industrial machinery. Our steel also needs to be re-melted
and turned into screws. Finally, our plastic needs to be melted from
pellets and reformed into a chair back. However, we cannot forget to
assemble all of our parts. We will likely screw the supports between the
legs, the legs to the base, and the back to the rest of the chair. Then,
we attach the polystyrene back, and paint all of it red. However, some
might choose to paint before assembly to get a full cover everywhere,
even if this does waste paint.

# Classwork

## Distribution

Considering this is an IKEA chair, it is more than likely that it
wouldn\'t be fully assembled in the factory, only assembled to point of
a flatpack. This is partially to make the products cheaper (no large
assembly machinery needed), and partially to increase space efficiency.
If you have a full chair, lots of the space it takes up will be air, but
this effect is lessened with a flatpack. It still has to be shipped,
which can have a large environmental impact, either via ship, or van or
both.

## Use

As a chair is used eventually, it likely will not have very much impact
on the environment. However, it will likely need a new coat of paint
before it is abandoned, and possibly two, depending on the age of the
end user.

## EOL

Eventually the chair **will** be scrapped or done with. It may be handed
on to a hospice, and or to a new family in need of a chair, but after
that either it will be sent to landfill, to possibly rot, and eventually
be burnt. Or the wood may be harvested and cleaned, to find acceptable
parts for new chairs (possibly in a \'Second Life\' range). The screws
might be cleaned and re-forged, and the plastic can also be recycled. I
am relatively sure about the plastic being recycled, but it is hard to
find other sources, as either the language used is vague (\'Designing
our products and services to enable a second life for products.
Eliminating waste throughout our own operations.\'), or hard to find in
a large report.
