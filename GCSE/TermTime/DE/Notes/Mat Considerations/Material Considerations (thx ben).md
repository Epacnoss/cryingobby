#dt #not-my-notes
## Papers & Boards

Paper thickness/weight measured in *gsm* (grams per square metre)
Common paper weights range from 80-150gsm
A weight greater than 170gsm is usually classified as a board, not paper
The thickness of boards is measured in *microns* (1/1000mm)

### Paper

- Layout paper
- Copier paper
- Cartridge paper
- Bleedproof paper
- Sugar paper

### Card

Around 180-300gsm 

### Cardboard

Two layers of card with another fluted sheet in between 
Available in thicknesses of 3mm upwards
Fluted construction makes it difficult to bend or fold
Resistant to knocks or bumps
Ideal for packaging
Good heat insulation (for food packaging / pizza)

### Board Sheets

Rigid card with thickness of 1.4mm and a smooth surface
White and black are most common colours
Often used for picture framing mounts or architectural modelling

### Laminated Board

Include various other materials that come in sheet form, like paper and cardboard

### Foam Board

Lightweight board that is made up of polystyrene foam sandwiched between two pieces of thin card or paper
Smooth surface, available in many colours and thicknesses
5mm most common
Ideal for modelling
Easy to cut and can be folded

### Styrofoam

Trade name for expanded polystyrene foam
Can be easily cut, shaped and sanded
Good heat insulation makes it ideal for wall insulation in caravans, boats and lorries

### Corriflute

Similar to carboard made out of high-impact polypropylene resin
Rigid and lightweight
Extremely waterproof
Easy to cut but can be difficult to fold
Often used for outdoor signs such as estate agents signs on houses
Also used for plastic containers, packaging and modelling

---

## Timber

### Hardwoods

Come from board-leafed deciduous trees that lose their leaves over winter.
Examples are birch, oak and teak
Grow slowly, and as a result the timber obtained from them tends to be dense, hard and heavy
Generally used for high quality items such as furniture

### Softwoods

Come from conifers - evergreen trees that keep their needles all year round
Examples are pine, cedar and spruce
Grow faster than hardwood and the wood is usually lighter in colour
Generally cheaper than hardwoods
Used in rooves, walls and doorframes

### Manufactured Boards

Sheets of timber made by gluing either wood fibres or wood layers together
Good way of making large flat boards that are stable
Natural timber can twist and warp
Examples are MDF, plywood, chipboard or block board

---

## Metals

Made by extracting metal ores from earth's crust by mining
Ferrous metals contain iron, and will corrode easily because of their iron content, unless treated with a suitable surface coat (paint, oil, wax)
Majority of ferrous metals are also magnetic
Non-ferrous metals are much more resistant to corrosion, and are more expensive
Shapes and sizes:
- Round bar
- Box section
- Tube
- T-section
- Angle
- Pipe
- etc.

### Ferrous Metals

Includes:
- Mild steel 
- Carbon steel
- Stainless steel
- Cast iron 
- Wrought iron

### Non-ferrous Metals

Includes:
- Tin
- Copper 
- Aluminium

### Alloys

A metal that is combined with other substances to make it stronger, harder, lighter, etc.

Includes:
- Brass
- Bronze
- Pewter
- Lead/tin soldier
- Stainless steel

---

## Polymers

Polymers are very large molecules made up of monomers
Can occur naturally or can be manufactured
Naturally occuring polymers include silk, wool, hair or even animal horn
Manufactured polymers are typically derived from petroleum oil

### Thermopolymers

Examples include PET, HDPE, PS, PP, ABS, acrylic and TPE
Soften when heated and can be moulded into shape
Harden again once cooled
Can be repeated many times

### Thermosetting polymers

Examples include polyester resin and epoxy resin
Undergo chemical change when heated to become hard
Once they have "set" they cannot be reheated and remoulded so they cannot be recycled

---

## Textile Fibres & Fabrics

### Natural Fibres

Examples include cotton, wool and silk 
Found in both plants and animals

### Synthetic Fibres

Examples include polyester, acrylic and nylon
Mostly non-biodegradable and therefore not sustainable
They can be engineered to give them a range of useful properties (flame resistance, crease resistance, stain resistance)

### Mixed / Blended Fibres

Can be mixed or blended together to improve quality, aesthetics, function or cost
Mixing is the process of adding yarns of different fibres together during the production process
Blending is the process of blowing different fibres together before they are spun into yarns

### Woven, Non-Woven & Knitted Fabrics

#### Woven Fabrics

Woven fabrics are produced on looms, and consist of warp and weft yarns
The warp runs vertically and the weft yarns are woven horizontally in an under/over configuration

#### Non-woven Fabrics

Non-woven fabrics are usually used for decorative or disposable products

**Bonded fabrics** are manufactured by applying pressure or heat / adhesives to bond the fibres together, and are often used in disposable textiles such as wet wipes, tea bags, surgical masks, dressings and nappies.

**Felted fabrics** are produced by applying heat, moisture and friction to fibres, which matt together.
Often used for decorative purposes and on the surface of pool tables

#### Knitted Fabrics

Made up of rows of interlocking loops, known as stitches. 
Most commonly used knits are weft and warp. 

---

## Modern Materials

Modern materials are materials that are continually being developed through the invention of new and improved processes. 

### Polymorph
- Comes in the form of polymer granules
- When heated to 60C, the granules melt and can be moulded into shape

### Teflon
- Mainly a non stick coating on cookware
- Also used in paints, fabrics, carpets and clothing to repel liquids

### Lenticular Plastic Sheet
- Used for visual illusions and "animation"

### Flexiply
- Form of plywood that is extremely flexible and can be easily bent into various shapes

### Precious Metal Clay
- Made from 99% silver or gold and 1% clay
- Can be shaped at room temperature then heated in a kiln to produce jewellery

### Conductive Polymers
- Plastic products that conduct electricity

---

## Smart Materials

Materials that respond the certain atmospheric stimuli (electricity, temperature, light)

### Shape-memory alloys / polymers
- Remembers its original shape when deformed, returns when heated
- Can be used in spectacle frames 

### Thermochromic sheet
- Printed with liquid crystal "ink" that changes colour above 27C
- Used in childrens toys, jewellery and temperature indicators


### Thermochromic Pigment
- Novelty mugs
- Can be added to plastics (e.g. clour changing baby feeding spoons to warn of hot food)


### Self-healing Materials
- Can detect and repair damage done to them
- Internal adhesive on cutting mats
- BioConcrete heals using bacteria that react with any water that gets into it and produce limestone to fill any micro-cracks 

### Composite materials
- Produced by bonding different materials to produce new materials with improved properties
- Glass reinforced plastic (GRP) is used in large structural items such as boats and car bodies
- Carbon fibre is similar to GRP but uses carbon fibres instead, making it even stronger and lighter. Used in protective helmets, high-end bicycles and sporting equipment
- Kevlar (similar to carbon fibre, but with even stronger plastic woven in) is used in bulletproof vests
- Laminates are materials made up of layers