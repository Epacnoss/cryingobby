#djamh
# Henry Jekyll 

-   Initially presented as a nice, but reclusive Doctor, but a PhD one rather than a medical one. Born into riches, and made more. 
-   Physically tall. 
-   Over the Novel, he starts quite happy but gets more depressed as the Hyde problem goes on, to eventually kill himself. 
-   Not much interaction with others - a recluse who only gets more reclusive (whilst Jekyll at least) as Hyde becomes worse. 
-   Jekyll wants to have fun, without the pressures of society or the damage to his own reputation, so he experiments and creates a concoction (the active ingredient is not what he thought, but an impurity in what he thought, so it cannot be recreated), to become another man who is his Evil side.  At first it works, but eventually Hyde comes out without the potion and Hyde starts to take over. He kills himself before it can get too much worse. 
-   Quotes: 
    -   “He began to go wrong, wrong in the mind,” (Lanyon about Jekyll) 
    -   “The large, handsome face of Dr Jekyll grew pale to the lips and there came a blackness about his eyes” 
    -   “You must suffer me to go my own dark way.” 
    -   “Like some disconsolate prisoner”. 
    -   “Weeping like a woman or a lost soul”. 
    -   “Pale and shaken and half fainting, and groping before him with his hands, like a man restored for death-there stood Henry Jekll”. 
        

# 'Edward Hyde' 

-   Smaller, Slighter, Younger than Jekyll; Ugly; Deformed/Decayed; Looks evil. 
-   When people see him, they feel shivers down their spines – people instinctually know that he is evil. 
-   Over the novel, he starts by committing petty crimes and playing well with Jekyll, before eventually he tries to take over Jekyll and kills people. 
-   He serves to advance forward the plot by committing heinous acts and trying to take over Jekyll, before eventually causing Jekyll to kill Hyde and himself. 
-   Quotes: 
    -   'Evil besides \[…\] had left on that body an imprint of deformity and decay' 
    -   'I sat in the sun on a bench; the animal within me licking the chops of memory; the spiritual side a little drowsed, promising subsequent penitence, but not yet moved to begin.' 
    -   'All human beings, as we meet them, are commingled out of good and evil: and Edward Hyde, alone, in the ranks of mankind, was pure evil.' 
    -   'O my poor old Harry Jekyll, if ever I read Satan’s signature upon a face, it is on that of your new friend.' 
    -   'If I am the chief of sinners, I am the chief of sufferers also.' 
        

# Mr Utterson 

-   Utterson is a lawyer and friend of Jekyll., He is presented as a respectable and wealthy man in Victorian London. He is rational and calm, but also curious. 
-   We see most of the novel from his perspective, and as of such we don't really know what he looks like. We only know that he is a white man (Mr Utterson, and racism was rampant). 
-   His role in the novel is to discover facts about the Hyde to let us discover facts about him, and to be a friend of Jekyll to help him later. 
-   He is liked. 
-   Quotes: 
    -   '"If he be Mr Hyde," he had thought, "I shall be Mr Seek."' 
    -   '"This is very good of you, this is downright good of you, and I cannot find words to thank you in."' 
    -   '"I can't pretend that I shall ever like him," said the lawyer.' 
    -   '“Some day...after I am dead, you may perhaps come to learn the right and wrong of this. I cannot tell you.”' 
    -   'O my poor old Harry Jekyll, if ever I read Satan’s signature upon a face, it is on that of your new friend.' 
    -   'It is one thing to mortify curiosity, another to conquer it.' 
        

# Dr Lanyon 
-   Dr Hyde is a Scientific Doctor, and used to be great friends with Dr Jekyll. However, they fell out after Jekyll's most recent experiment (the potion to turn into Hyde) was considered unscientific by him. 
-   As with Utterson, we don't see his physical appearance, and we can only assume that he is a white man. 
-   His role in the plot is to sow seeds of doubt in Hyde and to show the reader how repulsively ugly Hyde is. 
-   Quotes: 
    -   '"Unscientific balderdash."' 
    -   '"I am quite done with that person."' 
    -   'I saw what I saw, I heard what I heard, and my soul sickened at it; and yet, now when that sight has faded from my eyes I ask myself if I believe it and I cannot answer. My life is shaken to the roots.' 
    -   'The less I understood of this farrago, the less I was in a position to judge of its importance.' 
    -   'I sometimes think if we knew all, we should be more glad to get away.'