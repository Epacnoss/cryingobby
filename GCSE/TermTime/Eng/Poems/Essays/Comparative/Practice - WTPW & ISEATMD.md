#poems #analysis
Comparing **Where the Picnic Was** and **I started early - and took my
dog**

# Things to talk about:

-   The Journey - In Dickinson, we talk about a singular journey of a
    woman and her dog into the ocean, and in the Picnic Was, it is a
    journey if years for many people taking different routes.
-   In Dickinson\'s poem it is more like a dream, rather than in Where
    the Picnic Was it is more realistic.
-   In where the picnic was, we journey through time rather than space
    in Dickinsons.
-   In Dickinson\'s, it is a relatively concrete journey in her
    imagination, but it is told like we could experience it with her,
    whereas in WTPW it is a spiritual journey, where all participants
    went on similar journeys with different routes and different
    destinations.
