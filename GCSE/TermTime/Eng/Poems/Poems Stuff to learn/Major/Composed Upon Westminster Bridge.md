#major #quotes 
 
 # Poet
 - William Wordsworth, born in Lake District
 - Traveled around Europe as a young man, where he discovered the French Rev
 - Founded Romantic Movement
 - Made poet laureate
 - Focus on Nature/Emotion

# Quotes
 - *not anything to show more fair* - beauty of London, contrast to [[London]], similar to [[Adlestrop]]
 - *doth, like a garment, wear* - disguise, hiding the not fun of [[London]]
 - *silent, bare* - very quiet, night, silent almost like [[I Started Early - Took my dog]]
 - *Ships, towers, domes, theatres and temples lie* - list of things, like in [[Home Thoughts From Abroad]]
 - *sun more beautifully steep* - personifying nature - [[To Autumn]]
 - *at his own sweet will* - contrast to [[London]]