#major #quotes 
# Poet
 - Used to live in England.
 - (At time of writing), staying in Spain looking after his ill wife.
 - Went back to England post-death

# Quotes
[[TA_vs_HTFA.pdf]]
 - *whoever wakes* - like [[I Started Early - Took my dog]] hasn't
 - *whitethroat builds, and all the swallows* - **birb** [[Adlestrop]]
 - *blossomed pear-tree* - fruits of nature like [[To Autumn]]
 - *he sings each song twice over* - birds like [[In Romney Marsh]]
 - *fine careless rapture* - like death of [[To Autumn]]
 - *gaudy melon-flower*, *the little children's dower* - contrast to depression of [[Where the Picnic Was]]