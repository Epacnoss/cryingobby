#major #quotes  
[[TA_vs_HTFA.pdf]]

# Author
John Keats - famous poet and known druggie.
[[Keats Resarch.pdf]]

# Quotes
 - *maturing sun* - similar to [[Composed Upon Westminster Bridge]]
 - *conspiring*, *fill all fruit with ripeness to the core* - personification of Nature
 - *Summer has o'erbrimmed their clammy cells* - Food linked to [[Where the Picnic Was]]
 - *by the winnowing wind* - Gentle breeze like [[In Romney Marsh]]
 - *sound asleep* - dreaming like [[I Started Early - Took my dog]]
 - *the songs of Spring* - nice time of year - [[Home Thoughts From Abroad]]