#minor  #quotes

# Author
 - Born 1957
 - A radicalist
 - Lots of riots in that time
 - Industrial Revolution on doorstep
 - Hates child labour/prostitution

# Quotes
 - *each charter'd street* - shows oppression, contrast to [[Composed Upon Westminster Bridge]]
 - *mind forg'd manacles* - related to drugs etc, contrast to [[To Autumn]]
 - *hapless Soldier's sigh* - not being able to do anything, similar to [[Adlestrop]]