#gma-essay #gma-exercise #gma-written

Hier nous sommes allés à Concarneau. Nous sommes partis à dix heures, et
nous y sommes arrivés vers onze heures. D\'abord, nous sommes allés à la
plage. Nous y sommes restés jusqu\'à midi, et puis nous sommes retournés
à la place. Nous avons quitté la voiture sur la place, et nous sommes
allés au restaurant, où nous avons pris le déjeuner. Après le déjeuner,
nous sommes descendus au port, ou il y a toujours des garçons qui
attrapent des petits poissons. Maurice voulait aller à la pêche, alors
je suis allé avec lui à un magasin, où nous avons acheté une canne à
pêche. Maurice a attrapé plusieurs beaux poissons. Plus tard, nous
sommes allés au café, ou nous avons rencontré des amis. Nous avons
bavardé avec eux pendant une demie-heure, et puis nous sommes retournés
à la place et nous sommes montés dans la voiture.
