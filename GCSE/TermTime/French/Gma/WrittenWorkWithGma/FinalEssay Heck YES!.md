#gma-essay #french #gma-written #environment 
# Rough

## French Words to use

 - Internet
 - Les resaux sociaux
 - La television
 - Le portable
 - Ordinateur
 - Telecharger
 - les jeux videos
 - les achats
Or
 - En ce qui me concerne
 - Enfin
 - Mais
 - Et
 - Aussi, Ensuite, puis, en plus
 - En Revanche


## Plan

Hello Pierre,

I remember that in your last letter, you asked me about the advantages and disadvantages of the internet. I've got some ideas, and I'll speak about them now.

Firstly, I love buying things online because it is very easy and fast. I can also buy things when the shops are closed. I can also chat with my friends, which is very useful in a lockdown.

On the other hand, anyone can use the internet and do anything (including bad things like hacking). The internet is very addicting, which can be bad for your health.

I love to read the news and play video games with my friends on the internet. I have bought lots of things on the internet, like my computer.

# French
![[FinalEssay JustTheEssay]]

# Old Essay

Il y a beaucoup de moyens de transport. Quelques-uns, par exemple les bâteaux,
sont très vieux et quelques-uns, par example l’avion, sont très récents.
Mon moyen de transport préféré est l’avion parce qu’il faut aller à l’aéro-port,
et je les trouve très intéressants car il y a beaucoup de choses à faire, et beacoup
de magasins, et les prix sont plus bas que normallement.
Récemment je suis allé par avion aux Maldives, et c’était comme ci, comme
ça parce que les télévisions étaient fermées, et je n’avais pas de films sur mon
portable.
Pour le transport en commun, c’est bien car ça coute moins que le transport
privé, et c’est plus vite que la voiture car il n’y a pas d’embouteillage. En plus,
c’est meilleur pour l’environnement. Cependant, il y a les autres personnes, qui
peuvent être ennuyeux, et je trouve qu’ils font trop de bruit.
Je pense qu’il devrait y avoir plus de transport en commun, à cause de
l’environnement et nous devrions utiliser beaucoup moins les voitures qui
utilisent l’essence.

# Actual Essay
![[FinalEssayAcc]]
