# French

Salut Pierre,

Quand je lisais ta derniere lettre j'ai lu des questions que tu m'as poses sur les avantages and les inconvenients d'Internet. J'ai beaucoup d'idees, et je t'en parlerai maintenant.

Tout d'abord, j'adore acheter quelque choses en ligne parce que c'est tres facile et les choses arrivent chez moi tres vite. Je peux aussi acheter les choses quand les magasins sont fermes. Je peux bavarder mes amis sans aller chez eux, et c'est tres utile dans le confinement.

En revanche, toute la monde peut utiliser l'Internet et faire quelque mauvaises choses comme le piratage. L'Internet est un accro qui peut faire mal pour la sante.

Ce soir, je lirai les journaux sur mon portable et je voudrais faire la cuisine (en utilisant une recette d'Internet) ou jouer aux jeux videos. Avec ma famille, je vais telecharger un film et le regarder sur notre television.

A bientot,

Jack Maguire.