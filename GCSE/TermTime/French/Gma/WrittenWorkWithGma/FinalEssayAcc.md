# House/Home/Holidays Essay

Salut Pierre,

Merci pour ta derniere lettre. C'etait tres amusant a lire. Tu m'as pose des questions sur ma maison, et je voudrais t'en parler.

Elle est situee a Putney au sud-ouest de Londres. Elle est assez grande avec quatre chambres et quatre salles de bains en trois etages. Ma chambre est sur le troisieme etage, et je l'adore car c'est tres grand. C'est tres utile pour jouer aux jeux de realite virtuelle.

Ma famille aime aller a notre maison de vacances (elle n'est pas un gite, c'est plus comme une maison) au Pays de Galles. Elle se trouve au bord de la mer, et elle donne sur la mer, et la plage. Nous adorons jouer aux sports nautiques comme la planche a voile car c'est bon pour la sante et amusant. De temps en temps, nous faisons les chateaux de sable avec les personnes qui habitent pres de chez nous. La derniere fois, nous sommes alles au Pays de Galles en voiture parce qu'il y avait beaucoup de choses a y prendre, mais mes parents preferent prendre le train car ils ne doivent pas conduire et c'est bon pour l'environnement.

Cependant, si nous allons plus loin de chez nous, nous devons prendre un avion pour y aller. C'est mon moyen de transport prefere, parce qu'il y a beaucoup de films sur les avions. Quand nous sommes a Londres, nous utilisons les transports en commun parce qu'ils sont meilleurs pour l'environnement et ils coutent moins que la voiture.

Ou vas-tu passer tes vacances apres la coronavirus? Et, qu'est-ce que tu fais pour tes vacanes pendant le confinement?

A bientot

Jack
