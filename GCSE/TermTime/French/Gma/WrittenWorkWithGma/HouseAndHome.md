#gma-essay #gma-written
# Essay Prep - House&Home

J'habite dans une grande maison qui se trouve a Putney qui est une banlieue de Londres. C'est une tres vieille maison qui a ete faite pendant le dix-neuvieme siecle. J'habite ici depuis 2009. Il y a six ans, mes parents ont achete un gite au Pays de Galles, et nous l'avons beaucoup renove. Tous les ans nous y passons beaucoup de temps. 

Pour les grandes vacances les plus recentes, nous y sommes alles et nous faisions beaucoup de sports nautiques par exemple la planche a voile. Heureusement il faisait tres beau et il y avait beaucoup de soleil. 

Pour les grandes vacances prochaines, nous voudrions aller aux Etats-Unis, mais nous ne savons pas si nous pourrons y aller car nous ne pouvons pas faire les voyages maintenant a cause de la Coronavirus.