#gma-essay #gma-written
# School Rules

Mon ecole se trouve a Wimbledon dans le sud-ouest de Londres en Angleterre. Elle s'appelle 'Kings College School', et elle est tres grande. Il y a environs mille cinq cent eleves. Il y a beaucoup de garcons mais il y a aussi les filles en terminale et en premiere.

La journee scolaire commence a neuf heures moins cinq, et ce lundi, j'aurai un cours de physique et un cours de francais. Apres ca, c'est la recre. Nous aurons deux autres cours, et puis c'est le dejeuner. L'apres-midi, il n'y a pas de recreation. Je retourne a la maison en voiture avec des personnes qui habitent pres de chez moi.

Ma matiere preferee est la technologie car j'aime utiliser les machines et ma prof est sympa.

Apres l'ecole, je voudrais jouer aux jeux-videos avec mes amis, mais malheureusement j'ai trop de devoirs. En plus, j'adore faire la cuisine, et je le fais beaucoup quand je n'ai pas de devoirs.

La regle que je deteste la plus, est que nous ne pouvons pas utiliser nos portables pendant la journee scolaire. Mais je comprends pourquoi cette regle existe, et sans la regle, nous ne pourrions pas faire le travail.

Mon excursion preferee etait quand nouse sommes alles a PGL, ou nous avons fait beaucoup de choses amusantes mais nous avons mange les choses qui sonts tres mauvaises pour la sante.

Enfin, j'aime mon ecole, meme s'il y a des choses que je n'aime pas.
