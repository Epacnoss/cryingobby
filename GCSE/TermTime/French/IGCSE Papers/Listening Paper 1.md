#exam-practice #french #listening 
# Q1
1-3 are one part
4-6 are a different part
7-12 are another different part
1.	E
2.	B
3.	C
4.	B
5.	B
6.	C
7.	B
8.	A
9.	H
10. F
11. G
12. E
**12/12**
# Q4
1.	sept octobre
2.	quatre
3.	à vélo
4.	huit heures
5.	classique
**5/5**

# Q5
1.	P
2.	PN
3.	P
4.	N
5.	P
6.	PN
**6/6**

# Q6
1.	E
2.	A
3.	D
**3/3**

# Q7
1.	H
2.	B **C - bon marché = cheap**
3.	E
4.	D
**3/4**

# Q8
1.	B
2.	A
3.	B
4.	A
5.	A
6.	B
**6/6**

# Q9
A, D, E, F, G, I
**4/6** D/E is wrong, C/L is needed

# Q10
|                              | Avantages                                                      | Inconvénients                                                         |
| ---------------------------- | -------------------------------------------------------------- | --------------------------------------------------------------------- |
| **L'internet**               | les meilleurs prix; ne quitter pas la maison **needs acheter** | les magasins ne travaillent pas **pas de contact humain**             |
| **Les téléphones portables** | envoyer les textos en classe; plus en plus lége**r**s          | beaucoup d'accidents avec les voitures **Explain**. voler le portable |

**2 marks**
