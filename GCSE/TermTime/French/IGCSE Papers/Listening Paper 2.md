#exam-practice #listening #exam-practice 
**I got 41/50 - 82%**
# Le Temps
1 = Q1
1.	C
2.	A
3.	E

# Aux Magasins
1 = Q4
1.	B
2.	B
3.	A

# Les Films
1 = Q7
1.	E
2.	H
3.	B
4.	A
5.	D
6.	F

# Au Restaurant
Q13
1.	une voiture
2.	30
3.	poulet; frites	
4.	le service est génial
5.	souvent trop calme pour eux

# L'emploi
Q14
1.	B
2.	C
3.	B
4.	A
5.	B
6.	A

# Les jeunes et l'internet
Q15
| nom         | lettres |
| ----------- | ------- |
| **Manon**   | E       |
| **Ibrahim** | D       |
| **Tamar**   | A       |

# Aider à la maison
Q16
1.	I
2.	A **B**
3. H **A**
4. E 

# Les vacances d'Annie
Q17
1.	B **A**
2.	C
3.	B
4.	A
5.	B
6.	A **B**

# La vie scolaire
Q18
1.	C **B**
2.	B **A**
3.	C
4.	B
5.	C
6.	A

# Le cyclisme
| Aspects positifs                  | Aspects Négatifs              |
| --------------------------------- | ----------------------------- |
| On peut admirer le beau paysage   | on peut avoir mal si on tombe |
| toute la famille peut en faire    | **la météo**                  |
| Toute la monde se permettre **X** | **cout de velo**              |
| On peut en faire seule            |                               |
**3 pos, 1 neg - overall -3**