#exam-practice #reading #french #environment 
24/30 - Acceptable

# Q1 5/5
1.	D
2.	C
3.	E
4.	G
5.	A

# Q2 4/5
1.	B
2.	A **B**
3.	A
4.	C
5.	A

---

# Q3 4/5
1.	S
2.	T
3.	-
4.	F
5.	S **T**
6.	-
7.	F

# Q4 4/5
1.	médias
2.	loin
3.	beaucoup
4.	tristes
5.	peu **jouet - a toy**

---

# Q5 7/10
1.	Il habitait à son propre pays **Canada**
2.	Il n'avait pas de maison.
3.	Paul devait utiliser un fauteil roulant **Ils rient**
4.	Marianne n'a pas aimé que tous les personnages dans le film fumaient
5.	Dans la salle de film, il n'y avait pas de personnes handicapées.
6.	__
	1.	Elle a dû utiliser un escalier mécanique
	2.	Elle a dû ouvrir les grandes portes
7.	Les places réservées étaient trop près de l'écran
8.	Au premier rang **au rez-de-chausée**
9.	Elle a parlé à la personne qui est responsable du cinéma.