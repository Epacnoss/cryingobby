#exam-practice #reading #french #environment 
28/30 - YAY!

# Q1 5/5
1.	G
2.	F
3.	E
4.	D
5.	A

# Q2 5/5
1.	V
2.	F
3.	F
4.	V
5.	V

# Q3 4/5
B, D, F, G, J **E not D**

---

# Q5 5/5
1.	C
2.	C
3.	A
4.	C (?)
5.	A

# Q6 9/10
1.	plus de 20.000 **en 2007**
2.	tout le monde
3.	Pendant sa dernière visite à Paris
4.	le numéro de sa carte bancaire
5.	centaines
6.	à 50 mètres de son hôtel
7.	On doit taper son code sur le clavier
8.	deux heures
9. __
	1.	Il limite la pollution
	2.	Il réduit les embouteilages