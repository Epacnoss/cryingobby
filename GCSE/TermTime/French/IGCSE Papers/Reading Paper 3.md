#exam-practice #reading #french #environment 
27/30

# Q1 5/5
1.	B
2.	C
3.	G
4.	E
5.	D

# Q2 5/5
1.	C
2.	B
3.	A
4.	A
5.	C

# Q3 5/5
1.	dormi (B)
2.	grande (D) 
3.	mal (H)
4.	aider (G)
5.	avant (E)

---

# Q4 4/5
A, C, E, G, I **D, not C**

# Q5 3/5
1.	lire
2.	__
	1.	les filles n'aiment pas le fiction **ils lisent plus - en tête = ahead**
	2.	les garcons aiment les romans
3.	Ils peuvent obtenir gratuitement les toutes dernières chansons
4.	__
	1.	Ils passent le moins temps sur la lecture
	2.	Presque tous les jeunes aiment la lecture
5.	**\[2\]** Les parents doivent aimer lire, et ils doivent raconter des histoires à leurs enfants quand ils étaient petits.
6.	**\[2\]** Pas de livres chez lui, et une télévision dans sa chambre. **Other way round - 1mark**