#exam-practice #listening #french 
I had downloaded all of the listenings, but considering that I'll stop on the 26th, and I won't do any more on the 25th, I removed all audio files I didn't use.
# Part 4 - From infancy to Adulthood
## 4F - The World of Work
### Pg 194
![[4F_1_2 p194.mp3]]

1.	G
2.	V
3.	S
4.	G
5.	V
6.	S
7.	S
8.	G

### Pg 196
![[4F_2_2 p197.mp3]]

| Métier          | Avantages         | Inconvénients                  |
| --------------- | ----------------- | ------------------------------ |
| **facteur**     | *utile*           | *pas très bien payé*           |
| **journaliste** | beaucoup d'argent | beaucoup de lettres            |
| **acteur**      | être célebre      | pas utile                      |
| **médecin**     | bien paye         | on ne peut pas voir sa famille |

## 4G - Future Plans
### 198
![[4G_1_2 p199.mp3]]

| nom        | aprés le collège                          |
| ---------- | ----------------------------------------- |
| **Elodie** | *le brevet*, grand école                  |
| **Adrien** | institeur, s'inquiète, professionelle     |
| **Franck** | macon, le certificat d'aptitude, pratique |

### 200
![[4G_2_2 p201.mp3]]
Done in class

## 4H - Work, volunteering & Careers
### 202
![[4H_1_2 p203.mp3]]

1.	PN
2.	P
3.	PN
4.	N
5.	PN

### 204
![[4H_2_2 p204.mp3]]

1.	B
2.	C
3.	C
4.	A
5.	B
6.	A
## 4J - Communication
### 206
![[4J_1_2 p207.mp3]]

|             | Méthode préférée      | Avantage                         | Inconvénient                            |
| ----------- | --------------------- | -------------------------------- | --------------------------------------- |
| **Étienne** | *emails*              | *plus pratique que le téléphone* | on passe trop de temps devant l'écran   |
| **Sophie**  | textos                | vraiment rapide                  | beaucoup de textos coutent cher         | 
| **Enzo**    | messages instantieres | gratuit                          | il faut le wifi                         |
| **Carole**  | téléphone fixe        | tous ses amis l'utilisent        | les petits enfants preferent les textos |
