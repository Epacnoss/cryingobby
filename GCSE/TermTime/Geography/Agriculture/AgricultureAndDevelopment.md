#agriculture #development #geo #not-my-notes 
# Agriculture

^255808

## Food Shortages
### Causes 
![[Causes of Food Shortages]]
### Solutions 
![[Solutions for Food Shortages]]
### Effects 
![[Effects of Food Shortages]]

## Case Studies
### Intensive Farming in Swaziland
![[Intensive Farming In Swaziland]]
### Famine in Somalia
![[Famine in Somalia]]

## Soil
![[Soil]]

## Farming
![[Farming]]
### Inputs
![[Farming Inputs]]
### Types
![[Types of Farming]]

# Development & Globalisation
![[Geography/Agriculture/AnD Revision Docs/Globalisation]]
### Development Indicators
![[Development Indicators]]
### Reasons for a lack of Development
![[Reasons for a Lack of Development]]


## The 4 Sectors
![[Four Sectors]]
### Fisher Model
![[Fisher Model]]
