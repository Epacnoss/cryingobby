#agriculture 
-   Human Factors:
    -   Population increase which leads to greater consumption of food,
        increasing prices due to hight demand, and low supply.
    -   Increased pop. has also led to greater areas of land required to
        grow crops, destroying settlements and leading to overcrowding
        in certain areas.
    -   Destruction of forests has led to increased rate of c.c. asw
        well as desertification and prolonged droughts.
    -   Increased pop. has also led to increased pollution, as more
        people are using fuel to power cars/homes/gas for cooking.
    -   The resultant effect is an increase in water/air pollution,
        affecting the climate as well as food production.
-   Environmental Factors:
    -   Increased use of fossil fuels has increased levels of pollution,
        desertification, and eutrophication which has led to land
        degradation through loss of space and fertility of lands.
    -   Natural Disaster rates have also increased due to c.c., as well
        as their severity and length. Drought is the leading cause of
        food shortages in the world, and consecutive years of prolonged
        droughts have led to massive crop failures in Africa and Central
        America.
-   Economic Factors:
    -   Increased levbels of poverty in developed nations have meant
        that farmers are unable to invest in seeds, fertilisers or new
        technologies. This results in an increase of cheap, inefficient
        farming methods driving up food prices, meaning citizens cannot
        afford food, even if it is available.
    -   Global financial crises such as the Covid-19 pandemic have meant
        that foreign governments are unable to invest in developing
        nations and there is less availability for financial aid from
        groups such as the UN.