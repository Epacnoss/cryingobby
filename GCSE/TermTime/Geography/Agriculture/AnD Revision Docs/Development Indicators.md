#development 
A Country\'s rate of development and development level can be measured
using indicators of development. Examples of these are:

GNP Per Capita

:   The total value of goods and services provided by a country (within
    that country) in a given year, divided by the total number of
    citizens.

    -   But, the GDP per capita does not take into account the informal
        sector, sustainability of economic growth, or environmental
        sustainability.

Literacy Rates

:   The percentage of adults who can read and write.

    -   Literacy Rates do not take into account non-academic skills that
        are equally valuable (eg. farming techniques)

Life expectancy

:   The mean age lived by people in a countru at birth.

    -   Dramatic advancements in medicine and technology can mean that
        life expectancy for a certain age group is inaccurate.

HDI

:   A measure of social and economic development, based on: average
    years of education.

    -   HDI reflects long, not short-term changes, and may average out 3
        high indicators and 1 low one, providing a less accurate view of
        development.