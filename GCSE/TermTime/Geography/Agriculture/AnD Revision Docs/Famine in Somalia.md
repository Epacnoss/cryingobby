#case-study #agriculture 
-   Somalia has been affected by food shorages due to it\'s location in
    the Sahel. Drought in somalia has caused people to farm intensively,
    which exposes soils to high winds and rainfalls.
-   There is also some civil conflict in Somalia due to a disregad for
    authority, and a lack of infrastrucure from colonial rule. Citizens
    have taken up arms against the goverment, and have intercepeted
    international aid.
-   Local communities are unable to fund rudimentary healthcare systems,
    allowing diseases such as HIV or malaria to hinder their workforce.
-   The working population is also reduced, as lots of Somalian people
    are malnourished, lowering harvest and planting ability.
-   Famine has casued malnourishment and a weak workforce which is prone
    to disease.
-   Farmers gave over-farmed crops, which has ed to soil loss through
    wind erosion as well as desertification.
-   Children are tempted into becoming child soldiers, resulting in
    piracy, interception of international aid and a reduced workforce.
-   This has led to the government abandoning investment in sustainable
    infrastructure, causing further malnourishment and political
    instability.
-   Food shortages have been combated by creative, simple irrigation
    systems by containers at the base of rocky outcrops and hillsides to
    catch water.
-   International Aid has been given to Somalia, and equipment needed to
    improve agriculture has been provided.