#agriculture #farming
Labour
:   In LEDCs, farmers use abundant cheap labour instead if machinery
    whereas in MEDCs, where labour is more expensive (eg. UK), machinery
    is used. This results in a more efficient farming system, that is
    more intensive.

Capital
:   Capital can be used to increase the inputs into a farm (eg.
    machineryl, renewing buildings). This will increase yields, and can
    generate further profits for future investments.

Technology
:   Machines and irrigation can help to increase yields, as well as
    greenhouses with controlled temperatures, and genetic engineering.
    However, these new technologies wil require large amounts of
    capital.

-   Relief:
    -   Lowlands, such as flood plains, are fertile and good for growing
        crops.
    -   Steep slopes also prevent useage of machinery such as tractors,
        and have thinner soils that are more prone to soil erosion.