#agriculture #farming  
-   Physical inputs are naturally occuring things such as water or raw
    materials. There are many humans and cultural inputs, such as money,
    labour or skills. Inputs are **any** external source that can help
    in the agricultural cycle.
    -   Processes are the actions within a farm that allow the inputs to
        turn into outputs. Processes include milking, harvesting, and
        shearing. Processes greatly vary between farm types, and there
        is lots to do with either crops (eg. Ploughing, Planting), or
        animals (eg. Grazing, Lambing).
-   Outputs can be either negative or positive. Negative outputs include
    waste products, soil erosion, the leeching of chemicals into the
    soil. Positive outputs are the finished products, such as meat, milk
    and eggs, and the money gained from the sale of these products.
-   Feedback is the profit gained to be used as an input the following
    year, and the knowledge of manufacturing. negative or positive.
    Negative outputs include waste products, soil erosion, the leeching
    of chemicals into the soil. Positive outputs are the finished
    products, such as meat, milk and eggs, and the money gained from the
    sale of these products.
-   Feedback is the profit gained to be used as an input the following
    year, and the knowledge of manufacturing.