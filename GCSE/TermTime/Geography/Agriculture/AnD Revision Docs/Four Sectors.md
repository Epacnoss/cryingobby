#development 
-   Primary Sector:
    -   The extraction and collection of natural resources such as
        through quarrying, or activities such as farming and fishing.
    -   The primary sector tends to take up a large proportion of the
        economy in developing countries.
    -   It offers low-skilled, low-paying manual labour jobs.
-   Secondary Sector:
    -   Raw materials extracted by the primary sector are processed into
        manufactured goods.
    -   Exmaples of this include manufacturing, food processing and
        energy production.
    -   The secondary sector takes up a greater proportion of a
        developing economy (eg. India) than a MEDC.
-   Tertiary Sector:
    -   This is the service sector, and involves the selling of services
        and skills. It may also include selling goods from the primary
        and secondary industries.
    -   Examples include transportation, tourism, or retail.
    -   This is a more prominent sector in MEDCs.
-   Quaternary Sector:
    -   Information services such as IT, consulting and R&D are
        provided.
    -   This is also a service and is only present in MEDCs, in which
        employment is steadily growing.