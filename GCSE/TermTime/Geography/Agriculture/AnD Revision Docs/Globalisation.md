#development 
-   Globalisation is the process by which national and regional
    economies, societies and cultures have been integrated through the
    global network of trade, communications, immigration and transport.
-   Globalisation is caused by a number of factors:
    -   Improved transport that is more affordable, efficient and
        reliable.
    -   The adoption of transport containers, allowing for cheaper
        transport of greater goods.
    -   Improved technology, which makes the communication and sharing
        of information easier. It also reduces technological labour
        costs, as labour is often cheaper in Asia than Europe.
    -   Increased Mobility of labour has meant that people are willing
        to move between different countries in search of work, and has
        increased global trade remittances (which plays a large role in
        the development of a country).
    -   Growth of TNCs with a prescense in many different countries has
        allowed for more jobs, as well as an increase in the spread of
        global cultures and ideas.
    -   Growth of social medias such as Instagram, Twitter and Reddit
        have also allowed for increased spread of cultures and ideas, as
        well as increasing tourism, and highlighting key political ideas
        or movements across the world.
    -   Improved mobility of capital through the decrease in trading
        barriers has meant that capital can easily flow between
        countries and economies.
    -   This has also increased the connections between global financial
        markets.