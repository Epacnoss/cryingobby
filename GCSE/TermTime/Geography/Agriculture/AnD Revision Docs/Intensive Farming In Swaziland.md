#Case-study 
-   Swaziland has 4 physical input land zones:

    High Veld
    :   eroded and thin soils, steep slopes.

    Middle Veld
    :   rich soils, gentle slopes.

    Low Veld
    :   some rich soilds, flat land.

    Lubombo Uplands
    :   good, thin red clay soils, steep slopes.
-   Labour is typically from family members, with hand tools and little
    fertiliser used. Simple irrigation.
-   Cattle graze on Swazi National Land after April.
-   Pastures are burnt to get rid of coarse, dry grass.
-   Most farms are subsistence, with mostly maize grown, and few cattle.
-   Farmers are encouraged to join farmer\'s associations.
-   Harvested Maize is milled at the farmer\'s home to produce flower,
    which is then baked into bread to be eaten by the family. There is
    production of millet and vegetables, and the cattle provide meat and
    milk.
-   Erratic rainfall causes droughts, with water for irrigation not
    accessible to many. Farmers cannot afford to imprve, and roads are
    not good in remote areas, with soil erosion and animal disease
    rampant.
-   Investment is needed to improve irrigation, with use of crops that
    require less water. Farming inputs such as fertiliser or seeds
    should be subsidised.