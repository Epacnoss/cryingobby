-   Fertility is important in determininh the type of crops to grow and
    which processes to use (eg. the use and quantity of fertilisers).
-   Good Drainage systems reduce waterlogging, but require large amounts
    of capital to put in place and maintain.
-   Adverse weather conditions may mean thatharvest is delayed or
    destroyed resulting in a loss of capital (if the farm is
    commercial).
-   Wheat prices fluctuate according to supply and demand and so farmers
    must remain competitive, and continually invest in research of more
    efficient farming techniques and technologies.