-   Carbon emissions have to be reduced as well as pollution and
    deforestation in order to reduce the effects of c.c.
-   There is also a need to invest in clean, sustainable energy sources
    and aid needs to be given to developing nations to help implement
    this.