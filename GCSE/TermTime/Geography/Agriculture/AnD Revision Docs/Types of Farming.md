-   Arable/Pastoral Farming:

    Arable Farming
    :   Farms that only grow crops.

    Pastoral Farming
    :   Farms that only rear animals.

    Mixed Farming
    :   Farms that both grow crops and rear animals
-   Commercial/Subsistence Farming:

    Commercial Farming
    :   Farms that only produce food for sale.

    Subsistence farming
    :   Farms that only produce enough food for one farmer and his
        family (sometimes with a little left to sell).
-   Intensive/Extensive Farming:

    Intensive Farming
    :   Farms that use large amounts of machinery, money and technology
        or workers.

    Extensive Farming
    :   Extenive Farms have smaller inpus, but way more land.