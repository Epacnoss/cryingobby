#Case-study #climates 
Linked to [[Dubai Case Study Poster.pdf]]

# General
![[Dubai General]]

# Ecosystem
![[Dubai Ecosystem]]

# Industry
![[Dubai Industries]]

# Conservation&Management
![[Dubai Conservation & Manegement]]