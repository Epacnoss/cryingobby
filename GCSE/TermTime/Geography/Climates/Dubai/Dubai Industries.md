# Trade/Tourism
Lots of tourists who are all paying tax to allow Dubai to get off of oil. But, trade-wise importing more than exporting.

**In 2019
 - 16.73 million visitors
 - 75% Hotel Occupancy
 - 741 hotels
 - 3.4 nights per average stay
 - 126,120 rooms
 - 415 AED (£80) room price

Most visitors from Bangkok, Paris, then London**

# Construction
Lots of new buildings being built to sustain the city (> **30,000 cranes**). Problems with Migrant Workers having little work tho.

# Public Infrastructure
Lots of new public infrastructure (**eg. Dubai Metro - 75km of track, $8bn**)