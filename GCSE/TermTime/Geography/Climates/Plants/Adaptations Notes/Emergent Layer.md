#tropical-rainforest 
-   Direct heat from Sun.
-   F/F adapted to bright sun/strong winds/rain.
-   Tree branches thin (can\'t support animals)
-   Animals small, usually fly/glide.
-   A/I include :: birds/small monkeys.