#adaptation
Epiphytes are organisms that grow on the surfaces of plants and trees,
and get their moisture from the air around the plant it is attached to.
They have adapted to do this, as it is hard to get the sunlight at the
top without being on a tree.