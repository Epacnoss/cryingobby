#tropical-rainforest #adaptation 
The leaves on trees often have flexible bases to allow them to still
catch the sun, but to also allow the rain to fall onto the roots.