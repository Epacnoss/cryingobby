#adaptation 
Lianas are wooden vines that have roots in the ground (unlike
Epiphytes), but use trees as vertical support (like Epiphytes) to grow
leaves nearer the sun.