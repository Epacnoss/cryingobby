#tropical-rainforest 
The majority of all the nutrients are stored in the biomass, rather than
the soil, meaning that when farmers take down all the trees and burn
them, the plants don\'t grow.