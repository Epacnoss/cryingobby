#tropical-rainforest #adaptation 
Lots of trees have thin smooth bark, in order to make it harder for
insects and plants like epiopytes to gain a foothold, and when they do
get a foothold, it is easier to shed to get rid of parasites.