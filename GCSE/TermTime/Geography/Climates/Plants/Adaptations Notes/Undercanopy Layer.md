#tropical-rainforest 
-   V. humid + damp.
-   Many plants + mosses/lichens/fungi grow in this layer.
-   Insects found here due to very little seasonal change.
-   A/I include :: Beetles, Butterflies, Jaguars & Snakes