#geo #tropical-rainforest 
# Task 1: Main Characteristics of Each Layer
## Emergent:
![[Geography/Climates/Plants/Adaptations Notes/Emergent Layer]]

## Canopy
![[Canopy Layer]]

## Undercanopy
![[Undercanopy Layer]]

## Forest Floor
![[Forest Floor]]


# Task 2: Why so much biodiv?
![[Why so much Biodiv]]

# Task 3: S/B + Nutrient Transfers
![[Nutrient Transfers]]

# Task 4: Animal Life in the Rainforest
![[Animal Life in the Rainforest]]

# Task 5: Adaptations
## Fast Growing Trees
![[Fast Growing Trees]]

## Epiphytes
![[Epiphytes]]

## Thin, smooth bark
![[Thin Smooth Bark]]

## Lianas
![[Lianas]]

## Flexible Leaf Base
![[Flexible Leaf Bases]]
