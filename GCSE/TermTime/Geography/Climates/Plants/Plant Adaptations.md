#adaptation #tropical-rainforest 
# Plant Adaptations
| Adaptation                     | Explanation                                                                                                                                                                     |
| ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Long Tap Roots (phreatophytes) | They can tap into underground water sources that would otherwise be difficult to reach, which are useful for when there is little rain.                                         |
| Horizontal Roots               | They also may extend the roots horizontally to trap any water that falls onto the surface of the desert.                                                                        |
| Small in Size                  | This reduces the amount of water lost from the surface of the plant.                                                                                                            |
| Thorns instead of Leaves       | Many desert plants have thorns rather than leaves to discourage animals from eating them.                                                                                       |
| Fleshy/Spongy                  | Any rain that falls can be stored here rather than lost, which means that the plants will still have water during the more dry seasons.                                         |
| Waxy Leaves/Thorns             | Most Desert Plants either have thorns or waxy leaves to reduce transpiration.                                                                                                   |
| Adapted seeds                  | Lots of desert plant seeds will lie dormant on the sand until enough rain comes for them to germinate - it takes a lot of energy to make seeds, so the plant must not waste it. |