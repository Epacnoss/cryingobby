#landform #coasts 

 - **Concordant Coastlines** are where bands of hard and soft rock run **parallel** to the sea.
 - **Discordant Coastlines** are where bands of hard and soft rocks run **perpendicular** to the sea.

Discordant coastlines cause **differential erosion**. This is due to wave refraction, where waves bend to converge at headlands, and diverge at bays, which is why we get [[Deposition]] at Bays and [[Erosion]] at headlands.