#coral
**Corals survives because they have a symbiotic relationship with algae. The coral gets a vast majority of its food from Algae. The algae needs a stable place to live**

 - **Sunlight:** Corals need to grow in shallow water where sunlight can reach them. Corals depend on Sunlight for oxygen, and Corals rarely develop in water deeper than 50m.
 - **Clear Water:** Corals need clear water that lets sunlight through; they don't thrive well when the water is opaque. Sediment and Plankton can cloud water which decreases the amount of sun that reaches the algae (which need the sun for photosynthesis). Corals are sensitive to pollution and sediments. Sediment can create cloudy water which blocks out sun and can harm the polyps. Wastewater discharged into the ocean near reefs can cause seaweeds to overgrow the reef.
 - **Consistent Water Temp:** Reef-building corals require warm water conditions to survive. Different corals living in different regions can withstandvarious temperature changes, but they generally live in temperatures of 18-27 Celsuius.
 - **Oxygen and water of consistently high salinity.**