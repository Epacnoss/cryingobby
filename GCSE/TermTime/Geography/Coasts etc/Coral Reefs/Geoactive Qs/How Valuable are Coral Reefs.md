#geoactive #coral 
Coral reefs are incredibly valuable for lots of reasons:
 - **Biodiversity:** It is estimated that Coral Reefs contain more than 1,000,000 species with 4,000 types of fish and 400 species of coral.
 - **Medical Treatments:** Already Coral Reefs organisms are being used for treatments for HIV and Cancer. 1/2 of all research into new cancer drugs is marine-related, and we hope to find more cures with more research.
 - **Fisheries:** Coral reefs form the nurseries for about 1/4 of the world's fish stocks, and thus lots of revenue for local communities and fishing fleets. An estimated 1,000,000,000 people have a dependence on coral reefs for food/income. If properly managed, reefs can yield 15 tonnes of fish and other seafood per km^2 per year.