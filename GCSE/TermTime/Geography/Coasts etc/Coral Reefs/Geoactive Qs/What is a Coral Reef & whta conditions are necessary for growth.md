#geoactive #coral 
Corals are very sensitive to change and their environment and thrive in the following conditions:
 - Steady temperatures of between 20->30 degrees all year round.
 - Shallow seas, up to 40m in depth.
 - Clear, clean water with a steady salt content.

Coral reefs get damaged when these conditions are changed by 1 or 2 degrees, or more pollution or algae.
 We can tell how healthy coral reefs are by watching the colours - organisms called zooxanthellae live inside the polyps and produce food via photosynthesis and give part of it to the coral, and corals react to environmental stress by ejecting the zooxanthellae which give the coral reefs the colour. Then, when we see the coral reefs bleaching we must know that the zooxanthellae are being ejected from environmental stress.