#geoactive #coral 
There are 2 main threats - Human & Natural.
# Natural Threats
 - **Hurricanes:** The Carribean regularly suffers from hurricanes with strong winds and heavy rainfall, and large waves caused by hurricanes can break or flatten coral heads and slow-growing corals may be overgrown with algae before they can recover. Heavy rainfall increases surface runoff and sediment entering the sea.
 - **Global Warming:** The increased atmospheric CO2 levels can lead to the sea water being more acidic. This makes it more difficult for corals to produce Calcium Carbonate for their skeletons and can even cause the existing Calcium Carbonate to break up.
 - **Coral Diseases:** About 30 diseases have been identified since they were first discovered 30yrs ago, and Coral disease has a major impact on Carribean reefs where 80% of coral has been lost to disease in the last 20yrs. There is little known about the causes, but high temperatures are thought to be linked.
# Human Threats
 - **Over-fishing:** Fishing is a very important occupation in the Carribean but lots of overfishing has taken place which has caused unbalance in the special ecosystems in the Reefs. If too many fish are caught then algal blooms can happen which rapidly decreases the amount of sun available for the zooxanthellae which reduces the food for the coral, and lots of the coral reefs get replaced by algal ecosystems. 
 - **Destructive Fishing:** Sometimes fishing trawlers drag nets, use explosives or even  spray the sea with cyanide. These methods can stress and kill the coral polyps. 
 - **Boats:** Fishing & the tourist boats can also damage the coral reefs by leaking fuels.
 - **Global Warming:** The Main cause of Global Warming is people - see in the Natural Threats for reasons why this is bad.
