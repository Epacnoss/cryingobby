#coral 
It is a line of **coral polyp** found in warm shallow waters.
Coral reefs are built from the limestone remains of Coral Skeletons and coralline algae - but corals are not restricted to the tropics.
They thrive in temperate seas, in the cold blackness of deep oceans and even under polar ice but in the absence of warm water and symbotic algae, they are unable to grow into reefs.