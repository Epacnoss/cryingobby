#holderness-coast #Case-study #coasts 

# Geology & Landscape
![[HC Geology & Landscape]]

# Coastal Erosion
![[HC Coastal Erosion]]

# Bays of the Future
![[HC Bays of the Future]]

# Coastal Monitoring
![[HC Coastal Monitoring]]

# Who's In Charge
![[HC Who's In Charge]]

# SMPs/Protection
![[Explain how SMPs work]]

# Other Areas that get protected
![[Why are Mappleton & Easington Worthy]]