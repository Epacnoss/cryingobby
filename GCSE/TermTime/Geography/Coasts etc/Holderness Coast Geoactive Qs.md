#geoactive #coasts #Case-study #geoactive 
# Holderness Coast Geoactive
## Explain how the geology of the Holderness Coast puts it at risk of coastal erosion.
![[Explain how the Geology puts it at Risk]]

##   Explain how Shoreline Management Plans work and why they are beneficial.
![[Explain how SMPs work]]

## Explain why Mappleton and Easington are considered worthy of Coastal Protection.
![[Why are Mappleton & Easington Worthy]]

## Explain how the coastline around these settlements is protected and the impacts (both good and bad) that this may have.
![[Explain how the coastline around these settlements is protected and the impacts (both good and bad) that this may have]]