#Case-study #coasts #holderness-coast #geoactive 
1.    **What is an SMP and what do they take into account**
2.    **What four options do they consider and what was the outcome of the SMP for the Holderness coastal stretch?**

SMP's provide a detailed assessment of the risks associated with coastal processes. They set out policies to reduce risks to people and as well as to the natural environment. One of the basic principles is that natural processes should not be interfered with save for protecting life/property.

There are four coastal defense options that make up the final decision of any SMP:
1. Do nothing (let the erosion take its toll and control the damage)
2. Hold the existing defense line by monitoring or improving the level of protection (implement some Soft or Hard Engineering [[Managing Coastal Erosion#^269b84]]).
3. Advance the existing defense line. 
4. Retreat the existing defense line.
 
Holderness took the 'do nothing' approach in areas that aren't already protected and they are taking 'hold the line' in areas where defences are implemented.