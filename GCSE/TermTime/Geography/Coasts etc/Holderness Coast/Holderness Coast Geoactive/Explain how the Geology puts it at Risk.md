#Case-study #holderness-coast #geoactive 
1.    **Consider rock type and the relationship between rock type and wave energy, REMEMBER – mention specific rock types.**

The area of Holderness didn't exist before the ice age and most of the area is less than 12,000 years old. It is mainly made of boulder clay which is the debris carried from glacial advances. It is easily weathered and eroded, because it formed under a small amount of pressure, rather than the large amounts of heat and pressure that most rocks form under. More often than not, you can break it up in your hands which illustrates how easily it is eroded.

Whilst Holderness Coast is on the East Coast rather than the West Coast (the West Coast has a large fetch all the way from the US, whereas the East Coast only has a fetch from Western Europe), it does get lots of storms from the Atlantic currents going to the North Sea, storms which erode it.
