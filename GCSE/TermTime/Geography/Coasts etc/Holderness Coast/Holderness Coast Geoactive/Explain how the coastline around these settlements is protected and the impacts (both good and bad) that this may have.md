#Case-study #holderness-coast #geoactive 
1. **Think about the types of coastal protection that have been implemented and where**
2. **Can you link these to the approaches suggested by the SMP?**
3. **Hard and soft engineering bring benefits but they also bring impacts that are detrimental – have you considered all of these in your answer?**

 - **Bridlington** has 3.6km of high hasonry, concrete seawalls and groynes to stabilise and protect the beaches. The SMP reccomends that this stays and that if further protection is needed, it will be provided due to the size of the settlement to maintain their coast.
 - **Hornsea** has 1.86km of cocrete seawalls, groynes and rock armour. Recent upgrades have increased the heights of the seawalls to cope wth rising sea levels. They have also added wave-return profiles to parts of the seawalls and strengthened the groynes to increase the size of the beaches. As with Bridlington, the SMP reccomends to keep upgrading the defences to keep the coastline and protect Hornsea.
 - **Withernsea** has 2.26km of concrete seawalls, timber groynes, rock armour and an offshore rock armour reef (rather than stopping [[3. Longshore Drift.pdf]], or stopping waves or building up the beach, offshore reefs make the waves break earlier onto water to take their energy out onto the sea rather than a wall. These are also effective at becoming actual reefs in the right conditions which is good for biodiversity). It was last upgraded by adding re-curved sections of seawalls with rock armour protection.

Here, a majority of the techniques used have been hard engineering which is incredibly effective, but does have some disadvantages such as the fact that it is incredibly expensive (hundreds of pounds per metre) and it can harm the coastlines in other areas.

Legislation has been implemented in other neaby areas of the Holderness Coast, such as the *'roll-back policy'*, where there are limits on how close you can build to the cliff (200m, and even at that distance you need permission. Picked from a rate of erosion of 1.5-2m per year for 100 years of use). Existing infrastructure will slowly be destroyed/relocated as the cliff comes closer.