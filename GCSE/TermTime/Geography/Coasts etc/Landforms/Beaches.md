#landform #depositional #coasts 

Beaches form in sheltered environments, such as bays. When the swash is stronger than the backwash, deposition occurs.