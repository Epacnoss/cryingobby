#landform #erosional
- The erosion of a cliff is greatest where large waves break
 - Here, **[[Hydraulic Action]]**, abrasion & wave pounding actively undercut the foot of the cliff forming an indent called a **wave-cut notch** whilst the cliff face is also affected by abrasion as rock fragments are hurled against the cliff by the breaking waves.
 - This **undercutting** continues and eventually the **overhanging cliff collapses** downwards - this process contuinues and the clif gradually retreats and becomes steeper.
 - As the cliff retreats, a gently-sloping rocky platform is left at the base, this is  known as a **wave-cut platform** which is exposed at low tide.
 - This platform is often smooth and slippery as it is **abraded** by the material which collapsed from the cliff face.
 - The continual collapsing of unsupported cliffs as a result of the formation of **wave-cut platforms**, causes **cliff retreat** 