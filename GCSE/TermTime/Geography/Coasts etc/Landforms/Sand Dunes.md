#depositional #landform #ecology 

1.	Sand dunes are coastal landforms produced by the wind, which blows across a dry sand surface and is usually able to **transport sand grains**
2.	When the wind drops, sediment is **deposited** and usually, sand **accumulates** around an **obstruction** such as bush, driftwood and rock.
3.	As more sand particles are deposited, the dunes grow in size.
4.	**Vegetation**, such as **Marram Grass** or **Lyme Grass**, **colonises** the dune and their roots help to bind the sand together and stabilise the dune.
5.	Sand dunes **migrate** and **expand inland**  as the wind blows sand over the seaward side onto the landward side of the dune.
6.	This ultimately leads **succession**