#coasts 
# Managing Coastal Erosion
 - Coasts are vulnerable locations that need to be protected - see [[Hazards & Oppurtunities]]
 - They need protecting because of the economic value they bring to areas e.g. fishing, tourism & transport
 - Coastal erosion is mainly caused by [[Hydraulic Action]], [[CorrosionSolution]], [[CorrasionAbrasion]] & Wave Pounding.
 - Weathering can also play a role.
 - Areas nearer to sea level and made from softer rocks are particularly vulnerable.
 - If erosion is allowed to happen, coastal roads, ports, holiday resorts, farmland & even whole villages may be lost.
 
 ## Sea Defenses

^269b84

  - **Hard Engineering:** the building of physical structures, usually out of wood or concrete to protect the coast. Hard engineering is usually the most effective, but it can be expensive and or ugly to look at. Also, by deflecting wave energy, it simply has to move somewhere else which doesn't really solve the problem.
  - **Soft Engineering:** rather than building physical structures made out of wood or concrete, soft engineering works with nature. The results often look much more natural and does not ruin the look of the coastline. It can also be cheaper, however the main problem is that soft engineering cannot withstand strong storms. A hurricane can strip a recently replenished beach of all its sand for example.