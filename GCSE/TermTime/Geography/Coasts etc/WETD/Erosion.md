#erosional 

Wearing down and removal of material.

 - [[Hydraulic Action]]
 - [[CorrasionAbrasion]]
 - [[Attrition]]
 - [[CorrosionSolution]] (also [[Chemical Weathering]] and [[Transportation]])
