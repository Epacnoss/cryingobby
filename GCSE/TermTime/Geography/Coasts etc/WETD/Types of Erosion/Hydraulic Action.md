This is the sheer power of the waves as they smash against the cliff. Air becomes trapped in the cracks in the rock and causes the rock to break apart.
