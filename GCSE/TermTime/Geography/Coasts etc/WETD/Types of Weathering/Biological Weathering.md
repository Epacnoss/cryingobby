Plants and animals can have an effect on rocks. Roots burrow into the rock, weakening the structure of the rock until it breaks away.
1.	Plant roots can get into small cracks in the rock.
2.	As the roots grow, the cracks become larger.
3.	This causes small pieces of rock to break away.