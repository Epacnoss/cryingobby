Similar to [[CorrosionSolution]]

Rainwater and sea water can act as weak acids. If a coastline is made up of rocks such as limestone and chalk, over time they can be dissolved by the acid in the water.
