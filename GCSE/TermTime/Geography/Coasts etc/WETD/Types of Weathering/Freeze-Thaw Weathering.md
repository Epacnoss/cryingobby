Freeze-thaw weathering occurs when rocks are porous (contain holes) or permeable (allow water to pass through).
1.	Water enters cracks in the rock.
2.	When temperatures drop, the water freezes and expands causing the crack to widen.
3.	The ice melts and water makes its way deeper into the cracks.
4.	The process repeats itself until the rock splits entirely.