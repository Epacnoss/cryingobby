 - 2nd largest market after USA.
 - Shops in >300 cities.
 - 206 factories, 260,000 workers, 70% female, average age 26
 - **Used to have:** Abundant supply of labour, cheap costs, **but now** more expensive due to 1970s **one child policy**.
 - Factories finding it difficult to find workers. Lots of relocation to [[Nike Why SEA]].

| Salary | China | Indonesia | Vietnam |
| ------ | ----- | --------- | ------- |
|        | $500  | $300      | #250    | 
