#energy 
# How and why the UK's energy mix has changed since 1970
![[How Energy changed since 1970]]

# The advantages and disadvantages of nuclear power
![[ProCon of Nuclear Power]]

# Types of renewables found in the UK
![[UK Renewables]]

# Reasons why nuclear and renewables are likely to be more important in the future
![[Reasons NukeRenewable more likely to be important in future]]