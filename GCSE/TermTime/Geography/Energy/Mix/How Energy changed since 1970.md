#energy 
In 1970, we used predominantly fossil fuels (38% coal, 48% oil, and 8%
gas), with renewables being a small bit off to the side (3.5% nuclear
power, \<2% hydroelectric power, \<2% renewables and \<1% biomass). This
was because we weren\'t aware of the negative effects of fossil fuels in
the same way. In 2014, however, we had a lesser dependence on specific
types of energy, even if it was still mainly fossil fuels. However,
there are fewer fossil fuels being used. We had 30% coal, 30% gas, 19%
nuclear, 9.5% wind, 6.8% bioenergy, 1.8% hydro, 1.2% solar and 1.8%
other.