#nuclear
As more and more of our fossil fuels are consumed, and we rely on them
more and more, eventually they will run out. That, and also the fact
that we are facing a climate disaster, of which one of the major causes
is our addiction to fossil fueled power plants. This means, that over
the next few years rather than the next few hundred years, we will have
to switch completely to renewable or low-carbon power sources.
Renewables are important because they go for the forseable future, and
they don\'t pollute our environment nearly to the same degree (even if
they still do pollute a bit, as lots require a lot of concrete).
However, our renewable technology is not quite ready for a transition
yet, so lots are proposing we use nuclear energy as a middle man to stop
polluting the environment, but still have clean, reliable energy until
we have all of our other renewables completely worked out. However,
nuclear fission must be used as a middle-man, rather than a long-term
solution, because right now we do not have a long-term solution to the
nuclear waste.
