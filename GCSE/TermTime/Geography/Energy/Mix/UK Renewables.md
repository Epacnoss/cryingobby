#energy #renewable
In the UK, we have wind, wave, hydro, biomass and solar, although
biomass is recyclable rather than renewable. We have lots of offshore
wind plants, as well as ones on land. Unfortunately, Solar isn\'t suited
to the UK due to a lack of consistent sun, although that makes wave/wind
plants all the better. We also don\'t have enough rivers shaped the
right way for hydro plants, although we do have the Electric Mountain in
Snowdonia in Wales.

-   Our offshore wind plants can generate as much power as the rest of
    the world combined, and there are currently 28 farms with 1500
    turbines and 5GW. There are over 900 onshore wind farms with 5000
    turbines and 8.5GW of power.
-   Biomass can be sourced from lots of living substances like food
    waste or animal manure, and it can even be burnt in conventional
    power stations. It can also be made into biodiesel or biogas.
-   Hydro-electric power is about 1.5% of the UK\'s electricity
    production, and there are lots of types ranging from dams to pumping
    water. Electric Mountain is the UK\'s largest Hydro-electric plant.
-   There are only a few wave and tidal power plants here in the UK, but
    they are expected to make a bigger contribution this and next year.
-   Even if we only gain 1.2% of our energy via solar power, we have had
    massive growth recently mainly due to government incentives.
    Installations range from solar farms to panels on roofs.