#nuclear #energy 
-   Started in 1940s, post cold war.
-   Everyone went nuts thinking everything would be atomic.
-   Turns out it would be hard/expensive.
-   Plus v. risky, so people wouldn\'t invest.
-   Some didn\'t give up though.
-   Nuclear\'s finest hour was when oil prices skyrocketed, and more
    than 50% of the reactors were built between 1970 and 1985.
-   We all went with the Light Water Reactor, which is cheap, not too
    bad and already existing.
-   Really heavy elements get bombarded with neutrons, the atom explodes
    and causes a controlled chain reaction. Then, it is heated and
    turbined.
-   Not the safest, best or anything good tho.
-   Lots of disasters have happened with them.
-   Whilst in the 1980s, we had tons of plants being built... No none.
-   About 439 reactors in 41 countries.
-   160 new reactors planned rn.
-   More than 80% are light water.
-   Many countries are faced with a choice - renewables/old stuff OR
    upgrade plants.