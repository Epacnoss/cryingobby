#nuclear 
# Advantages

-   Saves lives - has prevented about 1.8 million deaths including
    fukushima etc.
    -   No coal mine accidents.
    -   Lots of dangerous stuff in a deep hole, or lots and lots in the
        atmosphere.
    -   Events singe in our minds, nuclear is like planes - only a few
        accidents, even if it works safely most of the time.
    -   Saves more lives than it destroys due to not as many air
        pollution problems.
-   Less CO~2~
    -   Many gigatons have not been released due to nuclear.
    -   Coal is cheap, but is bad/unsustainable.
    -   Compared to coal etc. it is clean. Maybe a middle man until we
        have better renewables.
-   Most of our current nuclear plants are outdated.
    -   Some new plants are incredibly efficient, or have dangerous
        materials for only a few hundred years, or be hard to turn into
        nuclear weaponry.
    -   Lets at least do some research.

# Disadvantages

-   Nuclear weapons proliferation:
    -   Easier to start/hide nuke development.
    -   Reactor tech has always been intimately connected with nuclear
        weaponry.
    -   There are even treaties which try to help distribute reactor
        technology without nuclear weapon technology, but it isn\'t
        doing well.
    -   5 countries have developed nukes with help of reactors
    -   The road to nuclear weaponry is paved with good intentions and
        safe, peaceful reactors.
-   Plutonium and other radioactive materials.
    -   Loses rads over 10s of thousands of years.
    -   Can be reprocessed into nuclear weaponry, and nothing else
        because we don\'t have the right reactors (yet).
    -   No proper powers trying to keep nuclear waste safe - right now
        it gets dumped in a lead-sealed hole.
-   Nuclear Accidents
    -   Only 7 or 8 incidents, but incredibly dangerous when it does
        happen.
    -   Deaths in the 1000s, happening with different reactors in
        different countries in different times.
    -   How far do we go - how many people need to die, or how much land
        needs to be declared unfit for human use until we stop.