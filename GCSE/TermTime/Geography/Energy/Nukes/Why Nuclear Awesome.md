#nuclear 
-   Saves lives - has prevented about 1.8mn deaths including fukushima
    etc.
    -   Waste is normally stored, but ffs are pumped into the atmo.
    -   No coal mine accidents.
    -   Lots of dangerous stuff in a deep hole, or lots and lots in the
        atmo.
    -   Events singe in our minds, nuclear is like planes.
    -   Saves more lives than it destroys.
-   Less CO~2~
    -   Tons of gigatons have not been released due to nuclear.
    -   Coal is cheap, but is bad/unsustainable.
    -   Compared to coal etc. it is clean. Maybe a middle man.
-   Most of our current nukes are outdated.
    -   Some new are incredibly efficient, or have dangerous materials
        for only a few hundred years, or be hard to turn into nukes.
    -   Lets at least do some research.
-   Risks involved, but fortune favours the bold.
