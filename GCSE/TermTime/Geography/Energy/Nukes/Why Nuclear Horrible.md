#nuclear 
-   Nuclear weapons proliferation:
    -   Easier to make/hide nuke development.
    -   Reactor tech has always been intimately connected with nukes.
    -   There are even treaties.
    -   5 countries have developed nukes with help of reactors
    -   The road to death is paved with good intentions.
-   Plutonium etc.
    -   Loses rads over 10s of thousands of years.
    -   Can be reprocessed into nukes, and nothing else because we
        don\'t have the right reactors.
    -   No proper powers trying to keep nuke waste safe.
-   Accidents
    -   Only 7 or 8 incidents, but incredibly dangerous when it does
        happen.
    -   Deaths in the 1000s, happening with different reactors in
        differet countries in different times.
    -   How far do we go - where is the line.
