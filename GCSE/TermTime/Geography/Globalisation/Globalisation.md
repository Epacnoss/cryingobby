#globalisation #geo
# Globalisation
## Starter
**My definition of Globalisation**: Globalisation is the global flow (and spread) of products, ideas, cultures and peoples.
**Official Definition**: Globalisation describes a process by which national and regional economies, societies and cultures have become integrated through the global network of trade, communication, immigration and transportation.

## Types of Globalisation
![[Types of Globalisation]]


## Why is Globalisation Accelerating
![[Why is Globalisation Accelerating]]

## Poster
![[Maguire J Geo Globalisation Poster.pdf]]