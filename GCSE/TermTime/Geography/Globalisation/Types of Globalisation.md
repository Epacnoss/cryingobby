#globalisation 
-  **Economic**: Countries that trade with many others and have few trade barriers are economically globalised.
 - **Social**: A measure of how easily information and ideas pass between people in their own country and between different countries (includes access to internet and social media networks).
 - **Political**: The amount of political co-operation there is between countries.