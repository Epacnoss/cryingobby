#globalisation 
-  **Connections are lengthening** - new links between places that are greater apart.
 - **Connections are faster** - Faster speed of connections with people about to talk to one another in real time or travelling quickly between continents.
 - **Connections are deepening** - More people's lives connect with far away places - eg. purchasing commodities / cheaper travel. Not just rich people who 'live globally'
