#geo #landform #erosional #depositional 

# Coasts
![[Beaches]]
![[Cliffs]]
![[Sand Dunes]]

---

# Rivers
![[Delta Model Ans]]
![[Floodplain]]
![[Levees Formation]]
![[Meanders Model Ans]]
![[Oxbow Lake]]
![[Potholes]]
![[V-Shaped Valley]]
![[Waterfall]]