#bradshaw
As a river flows from source to mouth, it is belived to follow a set of changes in a range of key characteristics, and the Bradshaw Model predicts how these parameters are expected to change as you follow a river from Source to Mouth.

| Decreases going to Mouth | Increases going to Mouth      |
| ------------------------ | ----------------------------- |
| Load Particle Size       | Discharge                     |
| Channel Bed Roughness    | Channel Width                 |
| Slope Angle (gradient)   | Channel Depth                 |
|                          | Cross-sectional Area          |
|                          | Average Velocity              |
|                          | Hydraulic Radius (efficiency) |