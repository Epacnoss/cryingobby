#bradshaw 
# Decreases
 - **Load Particle Size** will decrease as the rocks are eroded by the river, via methods such as [[Hydraulic Action]] or [[CorrosionSolution]].
 - **Channel Bed Roughness** will decrease because of the same reasons the load particles get smaller - it is eroded.
 - **Gradient** will decrease as whilst the Sources of rivers are in mountains, the mouths are often in flat land. Once you reach the Lower Course, you don't need the gradient to push water, as the water has enough momentum and water behind it.
# Increases
 - **Discharge** is the volume of water passing over a point in  a second and is measured in cumecs or cubic metres per second. It will increase as more tributaries join the river. [[Factors that affect Discharge]]
 - **Channel Width, Channel Depth & Cross-Sectional Area** will all increase as the load of the river erodes the riverbed.
 - **Average Velocity** will increase as the river gains more water pushing behind it.
 - **Efficiency** will increase as the riverbeds are smoothed out, removing friction and increasing efficiency. [[Hydraulic Radius]]