#bradshaw 
- **Measuring Width.** A tape measure is placed where the water meets the bank on one side of the river. It is then pulled tight across the water surface between the banks. It is then measured in metres.
 - **Measuring Depth.** The width of the river is divided into 4 equal sections. Facing upstream and starting at the left bank, a metre rule is placed into the water at the bank, and the depth of the water is recorded. Repeat at 1/4, 1/2, 3/4 and the whole way across. Average the results.
 - **Calculating CSA.** Multiply the width by the average depth.
 - **Measuring Flow Velocity.** A metre rule is placed onto a bank of the river. A cork is dropped into the river upstream of the ruler.....