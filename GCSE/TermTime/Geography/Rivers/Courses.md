#river-courses #rivers
# Hydraulic Radius/Efficiency
![[Hydraulic Radius]]

# The Upper Course
![[Upper Course]]

# The Lower Course
![[Lower Course]]

# The Middle Course
![[Middle Course]]