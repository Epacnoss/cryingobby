#river-courses
Hydraulic Radius = CSA / Wetted Perimeter
If we have River A that is 5m deep, and 8m wide. (HR = 40/18). Then River Bthat is 2m deep, and 20m wide (HR=40/24). 
River B is much less efficient than Rvier A, and it might also start depositing more material, making it even slower.

The Higher the Hydraulic Radius, the more efficient the river.