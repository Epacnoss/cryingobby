#river-courses #lower-course 
The river is at its widest and deepest, and this is due to the large amount of water that the channel needs to carry.
Very little sediment.
Gravitational Potential Energy is low, but Kinetic Energy is incredibly high - lots of water, fine sediment, lateral flow reducing turbulence. 

During a flood, it reverses, and the Hydraulic Radius decreases as the river bursts its bank, due to friction from the banks. Don't get confused - the centre part of the river still flows incredibly fast. Lots of deposition occurs, because the water loses energy. That is the cause of floodplains - the water floods, dumps nutrients, the flood ends, and you are left with nice, flat, nutrient-rich floodplains.

Mainly **Depositional** Landforms.