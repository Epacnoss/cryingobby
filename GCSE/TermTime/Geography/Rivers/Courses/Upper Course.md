#river-courses #upper-course 
Rivers aren't interesting, until they flood.
In the Upper Course, the hydrualic radius is normally small, as the river is narrow and shallow, with lots or rocky sediment.

When it floods, more water comes, and the Hydraulic Radius increases, which can create interesting landforms. We also get more traction and saltation.

Mainly **Erosional** landorms.