#rivers #floods
- Precipitation: 
	 - More rainfall leads to a greater discharge - all precipitation that comes into the water basin will come out, and if the ground gets saturated with water, all of the remaining water will come via river.
	 - More rainfall may even cause a flood.
	 - Duration of rainfall - longer may mean that the water table gets high enough that no water can be infiltrated, increasing surface runoff.
	 - Antecedent rainfall (rainfall before), means that the stores are full, increasing surface runoff.
	 - Type of precipitation:
		 - Snow: that snow will melt, and if that happens fast, we will get more discharge.
		 - Hail: slower melting, decreasing discharge. Might even be slow enough to infiltrate all of it into groundwater storage.
 - Relief:
	 - If the river has lots of relief (ie. it is in the upper course), it will not likely have as much discharge - in the upper course most rivers are just streams, which combine to make bigger rivers in the lower course.
	 - However, if the river has lots of relief in the lower course, the river may slow down as it avoids hills.
	 - Steeper slopes lead to more surface runoff.
 - Climate:
	 - Some climates (tropical rainforests) can have lots of rain which can help to increase discharge.
	 - On the other hand, some climates (deserts) can evaporate away lots of water, or take it into groundwater storage before a large river can form, reducing discharge. ^87a962
 - Farming Practices:
	 - Some farming practices can be bad for the land, meaning that less water would be intercepted or taken into groundwater storage, increasing discharge.
	 - Ploughing creates channels which the water can run down.
	 - Further increasing discharge is when farmers remove trees, becuase then less water gets infiltrated by the trees, so more ends up in the river faster.
 - Rock types:
	 - Impermeable rocks mean that water cannot be infiltrated, resulting in more surface runoff and more discharge. ^f774eb
	 - Permeable rocks lead to less discharge, as more water is infiltrated with less surface runoff and less discharge.
 - Urbanisation:
	 - Urbanisation increases discharge, as all of the water leads to drains, which go to the river. Very little infiltration. See [[Factors that affect Discharge#^f774eb]] ^82bc1e
 - Weather:
	 - High temperatures increase evaporation rates on surfaces, which reduce discharge. [[Factors that affect Discharge#^87a962]]
	 - Long periods of extreme weather lead to frozen ground that cannot be permeated [[Factors that affect Discharge#^f774eb]]