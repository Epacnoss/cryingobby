#floods #flood-management #rivers 
# Flood Management
## Dams & Reservoirs
![[Dams & Reservoirs]]
## Floodplain Management
![[Floodplain Management]]
## Flood Prediction & Warning
![[Flood Prediction & Warning]]
## Straightening & Deepening
![[Straightening & Deepening]]
## Raising Levees
![[Raising Levees]]
## Flood Abatement
![[Flood Abatement]]