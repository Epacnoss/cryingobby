#flood-management 
- Dams are built along the course of a river to control the amount of discharge. Water is then held back by the dam, and it can be released in a controlled way, to mimic nature. The water can also be removed with pumps and sold to pay for the Dam.
- Water is usually stored in a reservoir behind the dam. This water can then be used to generate hydroelectric power, or for recreation purposes.
- They are however, expensive to build.
- Settlements and agricultural land may be lost when the river valley is flooded to form a reservoir.