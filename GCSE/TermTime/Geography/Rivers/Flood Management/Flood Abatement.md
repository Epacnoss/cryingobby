#flood-management 
- Flood Abatement is protecting existing infrastructure and buildings from future floods.
 - There are many ways to do this, ranging from lego-like blocks that are interlocked and positioned to resist the water, to flood walls.