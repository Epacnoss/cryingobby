#flood-management 
- The idea of floodplain management is to reduce flood risks, and to restore healthy ecosystems.
 - Floodplains once covered wide stretches along European Rivers, and the remaining ecosystems are very important to reduce flood risks.