#levees 
- Flood embankments are usually used in rural areas
 - They can take up a lot of space, and are cheaper than flood walls, but they increase the speed of water in the river which just moves the flooding downstream.