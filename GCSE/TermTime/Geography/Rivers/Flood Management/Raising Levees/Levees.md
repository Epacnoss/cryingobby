#levees
- Artificial Levees can be built along river banks so that if the river floods, the water will not be able to breach the wall and cause damage.
 - However, they are expensive and can spoil the looks of rivers.