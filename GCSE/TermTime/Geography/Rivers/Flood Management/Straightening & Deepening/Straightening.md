#straightening-deepening 
- A river channel may be straightened so that water can travel faster along the course. 
 - The channel course can also be altered, diverting floodwaters away from settlements.