#rivers 
# Intro
- Rivers present numerous threats and oppurtunities.
 - These threats can be managed by the amount of discharge in the river.
 - Remember, dicharge is the volume of water flowing down the river at any one time.
 - If discharge is too high then rivers will flood their surrounding areas, erosion will increase possibly destroying houses, roads and farmland and particularly in the tropics, diseases such as malaria and sleeping sickness may spread.

![[Factors that affect Discharge]]

# Advantages & Disadvantages

![[Oppurtunities of a River Mindmap]]