#lower-course #landform #deltas

Deltas are depositional landforms found at the mouth of a river where the river meets a body of water with a lower velocity than the river (eg. a lake or the sea). For a delta to develop:

 - The body of water needs to be relatively quiet with a low tidal range so that deposited sediment is not washed away and has time to accumulate.
 - Large amounts of sediment are brought downstream by the river.
 - Deposition then occurs in the lower course as the velocity of flow is reduced.
 - The mixing of saltwater and freshwater causes **flocculation** to occur where sediment gets stuck together. The salt held in solution joins sediment coming out of suspension and is deposited due to increased weight.
 - These deposited silt blocks cause **distributaries** to form as the river tries to reach the sea. Distributaries are smaller streams coming off the main river.
 - Deposited materials are not wahed away due to the low tidal range and a lack of current, so they continue to grow towards the sea. Eventually, they can be **colonised** by **vegetation**.