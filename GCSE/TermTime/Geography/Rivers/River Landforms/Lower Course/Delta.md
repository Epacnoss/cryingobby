#lower-course #landform #deltas #depositional 
A delta is an area of low-lying flat, marshy land where a river meets the sea or a lake.
Large amount of sediment are brought downsea.
Speed of flow is reduced and **flocculation** occurs where clay particles with salt particles, becoming heavier and depositing.
The silt blocks the course of the river.
The river course is then divided by the channel into distributaries.
The sediment is not washed away due to a lack of currents.

[[Delta Model Ans]]