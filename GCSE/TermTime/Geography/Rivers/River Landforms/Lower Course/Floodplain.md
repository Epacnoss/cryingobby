#lower-course #landform #depositional 
A flood plain is the flat land next to a river which is liable to flood. The flood plain could be less than 100m across in the case of a small river, or 100km across in the world's largest river valleys. Flood plains are often marshy and poorly drained.

Three types of deposition help the flood plain to grow:
Deposition of point-bars on the insides of meanders. The deposits are spread across the valley as the meanders migrate - both sideways and downstream.
The deposition of gravel from the river bed (part of the bed load)
The deposition of fine silt and mud (part of the suspension load on the flood plain itself when the river overflows its banks during floods).
A floodplain is a very fertile area due to the rich alluvium deposited by floodwaters.