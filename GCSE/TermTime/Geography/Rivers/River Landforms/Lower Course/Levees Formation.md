#lower-course #landform #depositional 
When the river floods, it slows down and loses energy. It begins to deposit the largest rocks (b), and the finer sediment gets deposited further out on the floodplain.
When the floods repeat more and more, more sediment gets deposited, and levees start to form. These are natural levees. There are also man-made levees to stop flooding. 
If the levees are breached, the water cannot get back down into the river, because it cannot get over the levees.