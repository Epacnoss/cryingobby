#middle-course #landform #meanders
 - The current will flow naturally in corkscrew motion known as **helocoidal flow**.
 - The fastest flow of the water (the **thalweg**) is on the **outside bend** and the river is deeper here, meaning there is less friction to overcome. This means that there is greater erosion on the outside bend as a result. The water will erode the outside bend as a result. The water will erode the outside bend via **hydraulic action** and **abrasion**. This causes the river to bend further and creates a **river cliff** which is **undercut** by the river.
 - The erosion is lateral/horizontal/side-to-side.
 - The river flows slower on the inside bend and the river is shallower here which creates **more friction**.  This results in less erosion and the sediment is brought from the eroded outside to be **deposited** on the inside of the bend, resulting in a **slip off slope/point bar**
 - Over time meanders become more pronounced or **incised**.