#river-tees #Case-study #rivers 
In **NE England**, **137km long**. Lots of **industry** at Teeside in [[RT Lower Course]], due to river allowing import/export of goods. The need for water has caused lots of reservoirs to be built along it like **Cow Green**.

 - Drains **1800km^2** of river basin.
 - Source is at Teeshead at 732m up