#geoactive #Case-study #river-tees 
[[5.0 River Tees GeoActive.pdf]]
# Questions
## Why does the river Tees flood?
![[Why does the river Tees flood]]
## What hazards does the river pose and why?
![[What hazards does the river pose and why]]
## What opportunities does the river present?
![[What oppurtunities does the river present]]
## Explain how is the river managed? (identify at least 4 methods explained and exemplified)
![[Explain how is the river managed]]