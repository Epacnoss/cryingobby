#Case-study #river-tees #river-courses #lower-course 
Very little bedload - mainly sand/estuary mud.
Lots of [[Meanders]] of all shapes and sizes. Max [[Erosion]] on the outside of the bends, leaving lots of [[Oxbow Lake]]s. One meander encloses a whole town - Yarn, which used to be one of the most important trading cities - 18km as the crow flies.

Eventually Stockton grew over, as it was closer. The Victorians couldn't be asked to deal with the [[Meanders]], and cut new channels.

Lots of industry around the Estuary - an Iron/Steel Works ([[Metal Extraction]]), an oil terminal and even a [[Nuclear]] power station.