#Case-study #river-tees #river-courses #upper-course 
Starts in the Moorlands of Cumbria, near County Durham. 600m above sea level, very little sun, lots of rain. 1200mm of rainfall - the bogs soak up water like a sponge. For over 1/2 of the year, it cannot hold onto the water, and the water moves around to eventually form a stream. 

Lots of reservoirs, useful for cities with less rain. Only 5km away from the source, all of the rocks are smooth. [[Erosion]] is the most powerful here - going down, lots of water, lots of bedload, and it cuts a [[V-Shaped Valley]].