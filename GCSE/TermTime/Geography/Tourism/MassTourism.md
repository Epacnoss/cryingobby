#tourism #benidorm #Case-study 
# Why Benidorm?
![[Features that Attract Tourists to Benidorm]]


# Positives and Negatives of Mass Tourism to Spain

## Positives:
  
| Economic                                                              | Social | Environmental                                                                                     |
| --------------------------------------------------------------------- | ------ | ------------------------------------------------------------------------------------------------- |
| Obviously, lots of tourism to Spain, so money for the Spanish people. | ?      | More people coming, means more income which means more money that can be spent on the environment |

## Negatives:

| Economic                                                | Social                                  | Environmental                                                                                                           |
| ------------------------------------------------------- | --------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| Can damage local business in the storm to help tourism. | Local traditions can also be destroyed. | More people flying in means more environmental damage, plus all of the single-use plastics in the hospitality industry. | 
