#Case-study #tourism #sarawak 

# General
![[Sarawak General]]

# Why Go?
![[Why go to Sarawak]]

# Risks
![[Risks of Tourism to Sarawak]]

# Sustainability
![[Sustainable Tourism in Sarawak]]

# Pros/Cons
![[ProsCons of Sarawak]]