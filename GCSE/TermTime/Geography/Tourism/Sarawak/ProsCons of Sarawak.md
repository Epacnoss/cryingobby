| Good                                                                   | Bad                                                                                       |
| ---------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- |
| Positive intl image can lead to more investment                        | Tourists may not respect local customs, dress inappropriately, or take insensitive photos |
| Better QOL for locals due to more money                                | Development for Tourists may use scarce resources like Water/Electricity                  |
| Locals may take greater interest in looking after the env for tourists | The culture may become *westernized* to welcome tourists                                  |
| Improvements in Local Infrastructure due to tourists paying tax        | More pollution/waste may come from tourists/tourist development.                                                                                          |
