Sarawak is a **state of Malaysia**, near the Equator. Tropical climate, and hot all year round, but with Wet/Monsoon seasons. Perfect for growing rubber/palm oil. Borders with Brunei, Sabah & Indonesia. Average temp is 25 C, with 200mm of rainfall per month.
**4,856,888 visitors**  in 2017, and they made up **7.9%** of the GDP. On average, they spent **54.3%** of their budgets on flying.

Ecotourism is important to Sarawak because in order to preserve it\'sheritage, it needs money, and better to accept tourism on their own terms, than to have some corporate giant swallow it up.