# Physical
-   Lots of rare wildlife like Orangutans.
-   Beautiful Beaches with clear water.
-   The Satang islands have reefs.
-   Huge Cave Systems.
-   The rainforests attract adventurers.
# Human
-   The Rainforest World Music Festival - it attracts over 20,000 people
    every year.
-   Diving, caving and a range of other activities.
-   There are many forts attracting historical and cultural tourists.
-   The Old state Mosque was built in 1852.
-   San Ching Tian Temple is the largest Taoist temple in SE Asia!