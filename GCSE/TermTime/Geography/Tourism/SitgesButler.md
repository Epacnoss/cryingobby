#Case-study #sitges #tourism 
# Context

Sitges is a long-established settlement , which occupies a coastal knoll
which sperates the two main beaches. The old town is inland, and was
originally a fishing village occupied since Roman Times. The railway
station opened in 1881.

# The Butler Model

Shows how resorts develop.

Discovery Stage
:   A settlement is \'discovered\' by a few people, who pass on their
    enthusiasm, and tell others to visit. Eventually, enough people
    visit to encourage more tourist related servives by the local people
    who see economic oppurtunity.

Growth/Development Stage
:   More and more people arrive, who may be attracted by things other
    than word of mouth, like brochures or travel guides. More services
    develop, and the resort gets bigger.

Success Stage
:   The resort becomes well-known and the town\'s facilities are fully
    occupied in the main season. It also might attract migrants who
    want/need work or holiday homes. Tourism becomes the main income
    source, often at the expense of earlier industries (like Fishing),
    which causes the local people to feel that they have lost their
    cultural identity.

Problem/Stagnation Stage
:   People become dissatisfied with the congestion of the resort, and it
    \'wasn\'t like it used to be\'. Then, people go toother destinations
    and the formerly successful resort goes into decline. Less visitors,
    less economic prosperity, and businesses close.
