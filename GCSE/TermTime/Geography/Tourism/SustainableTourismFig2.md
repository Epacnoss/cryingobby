#tourism #environment 
# Factors for making sure a sustainable destination stays:

## Vision:
-   Long term vision of future of destination.
-   Involvement of Stakeholders.
-   Prepare for Sustainable expansion.

## People:
-   Investment in training of staff so management roles can be
    undertaken.
-   Build up capacity within local staff to promote sustainability
    within attractions, accomodation etc.
-   Ensure health and safety of people working at destination.

## Environment:

-   Protect biodiversity.
-   Keep within carrying capacity of natural environment.
-   All wastes recycled/used.
-   Sustainable water practices - using grey water etc.
-   Work towards being 0 carbon.

## Infrastructure:
-   Accomodation designed to use less energy.
-   Access to destination - transfers - Boat? Coach?
-   Sewage systems - freestanding reed beds.
-   Solar, wind, biogas energy supplies.

## Local Community:
-   Local culture is respected and embedded in the destination.
-   Goods and services are brought from local community as a priority.
-   Feelings of ownership and pride in local resources encouraged.
-   Aim to strengthen community.
-   Lack of leakage of profits - remain in community.
