#blm
# East India Company - Jack Maguire

## Key Facts

 * It was founded on the 31st of December 1600.
 * It was made defunct on the 1st of June 1874, by the East India Stock Dividend.
 * It was founded by John Watts, and George White.
 * The HQ was in London.
 * They shipped Cotton, Silk, Indigo Dye, Salt, Spices, Saltpetre, Tea and Opium.

## A Summary of The Issue

At first, they were in it exclusively for the profit, and just took materials, and shipped them back to the UK. As the competition grew more fierce, they moved to other places, and amassed a private army of over 250,000 people to protect their interests (this was a fair bit larger than even the British Army at the time). Eventually though, after 1757, they started to take full administrative powers over their territories (in India, which grew the produce), and even started to tax them. Eventually, some of their ventures became less profitable due to the industrial revolution (textiles/fabrics), and the British wanted Chinese tea. Unfortunately, the Chinese traded in Silver, and since the UK operated on Gold, they would have had to get Silver via Continental Europe, which would be unprofitable. To make a profit whilst doing this, the EIC started to encourage farmers to farm opium, and the EIC sold it to private merchants which then sold it to the Chinese. The EIC now had money for buying Chinese tea. This went on for 50 years, before the Opium Wars when in 1839 the Chinese Government decided that all opium stock be handed to them for destruction.

Eventually, the Indians rebelled, and since they made up a large part of the army, they succeeded.

## Lasting Impact

The Lasting Impact of this is that the Indians have a rail system, ???

## Something I didn't know

That the Opium **wars** were started in order to get Chinese Tea, by a private company.

## Reflection Q

Whilst companies and people involved in the East India Company have done many Heinous Acts, they have tried to use that money for a good purpose, say Leeds University, or Guys Hospital. I believe that even if these institutions have come from bad money, that the statues should not be torn down - for if we tear down the statue, why not tear down the whole college? Instead, the fault should go partially to the lawmakers, who didn't do much for too long.
