#cults
# **B**ehaviour Control
 - Promote dependence and obedience.
 - Modify behaviour with rewards and punishments.
 - Dictate where and with whom you live.
 - Restrict/Control sexuality.
 - Regulate what and how much you eat and drink.
 - Deprive you of 7-9 hours of sleep.
 - Exploit you financially.
 - Restrict leisure time and activities.
 - Require you to seek permission for major decisions.
# **I**nformation Control
 - Deliberately withold and distort information.
 - Forbid you from speaking with ex-members and critics.
 - Discourage access to non-cult sources of information.
 - Divide information into **Insider** vs **Outsider** doctrine.
 - Generate and use propaganda extensively.
 - Use information gained in confession sessions against you.
 - Gaslight to make you doubt your own memory
 - Require you to report thoughts, feelings & activities to superiors.
 - Encourage you to spy and report on others *'misconduct'*

# **T**hought Control
 - Instill Black vs White, Us vs Them, Good vs Evil thinking.
 - Change your identity, possibly even your name.
 - Use loaded language and cliches to stop complex thought.
 - Induce hypnotic or trance states to indocrinate.
 - Teach thought-stopping techniques to prevent critical thinking.
 - Allow only positive thoughts.
 - Use excessive meditation, singing prayer, & chanting to block thoughts.
 - Reject rational analysis, critical thinking & doubt.
# **E**motional Control
 - Instill irrational fears of questioning or leaving the group.
 - Label some etions as evil, worldly, sinful and wrong.
 - Teach emotion-stopping techniques to prevent anger/homesickness.
 - Promote feelings of guilt, shame & unworthiness.
 - Shower you with praise and attention AKA *'love bombing'*.
 - Threaten your friends or family.
 - Shun you if you disobey or disbelieve.
 - Teach that there is no happiness or peace outside the group.