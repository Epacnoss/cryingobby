#cults #religions
# Religions vs Cults
| Religion                                                     | Cult                                                              |
| ------------------------------------------------------------ | ----------------------------------------------------------------- |
| Teachings based on ancient texts.                            | New interpretations of Holy books or new writings                 |
| Leaders advise on issues and interpret holy books.           | Leaders claim special knowledge or even to be god.                |
| Encourages questions and dialogue with other faiths.         | Discourages questioning and outside influences.                   |
| Gives guidelines for members to live by, based on scripture. | Uses mind control techniques to control the behaviour of members. |
| Allows people to criticise and leave.                        | Threatens those who speak out and prevents members from leaving.  |
| Works within a community.                                    | Isolates itself from the rest of the world.                       |                                                             |                                                                   |

See [[Bite Model]]
## Presentation Requirements
  - Key beliefs of the cult.
  - Key practices of the cult - how does it treat its followers?
  - Information about the leader, text and central building.
  - Do you, believe that is is a cult or a religion.


