#non-examined 
# Obsidian Vault
Mainly contains school stuff.

Reccomended editor is Obsidian.
Majority of notes are in Obsidian-Markdown (like markdown with a few extra features).
Lots of annotated pdfs as well.

# [Obsidian](https://obsidian.md) Notes

Obsidian works on a theory of Atomic Notes, where each note is about one thing, which I find nice.
It also acts like a *'second brain'*, in that you can link notes together, with tags and mentions. [[README]] is a link, ![[README]] is an embed link and #tag is a tag. Unless viewing in Obsidian, just ignore them.